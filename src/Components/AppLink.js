import React from "react";
import { Link } from "react-router-dom";
import { Icon } from "./Icon";
import { animateCSS } from "../Helpers/general";

export default function AppLink(props) {
  return (
    <Link
      to={"/" + props.link}
      className={"btn btn-primary m-2 col-sm-3"}
      onClick={_ => {
        let audio = new Audio(props.audio || "media/click.mp3");
        audio.volume = 0.5;
        audio.play();
        animateCSS("#pages", "zoomOut", function() {
          animateCSS("#pages", "zoomIn");
        });
      }}
      title={props.tooltip}
      {...props}
    >
      <Icon icon={props.icon} /> &nbsp; {props.title}
      {props.children}
    </Link>
  );
}
