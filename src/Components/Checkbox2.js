import React from "react";

export default function (props) {
  return (
    <div>
      <label className="hand float-left checkboxContainer">
        <input
          type="checkbox"
          checked={props.value}
          onChange={props.onClick}
        />
      </label>
      <span className="lineheight1 hand" onClick={props.onClick}>&nbsp; {props.title}</span>
    </div>
  );
}
