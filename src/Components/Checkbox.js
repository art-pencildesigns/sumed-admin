import React from "react";

export default class Checkbox extends React.Component {
  render() {
    const { onChange, value, id, help, children, className = "" } = this.props;
    return (
      <span className={"form-group hand" + className} onClick={onChange}>
        <span className="custom-switch">
          <input
            type="checkbox"
            className="custom-control-input"
            id={id}
            checked={value}
            onChange={onChange}
          />
          <label className="custom-control-label hand" htmlFor={id}>
            {children}
          </label>
        </span>
      </span>
    );
  }
}
