import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faBolt,
  faMobileAlt,
  faTrain,
  faGasPump,
  faCalculator,
  faDivide,
  faStopwatch,
  faDrumstickBite,
  faSave,
  faCopy,
  faBrain,
  faMehRollingEyes,
  faEnvelopeSquare,
  faSms,
  faPhoneAlt,
  faHome,
  faArrowLeft,
  faArrowUp,
  faArrowDown,
  faStar,
  faHorse,
  faBox,
  faFlag,
  faEllipsisH,
  faIdCard,
  faPercentage,
  faUserFriends,
  faSun,
  faPercent,
  faArrowCircleLeft,
  faEgg,
  faClock,
  faMapMarkerAlt,
  faLaughWink,
  faBusAlt,
  faTrashAlt,
  faPause,
  faPlay,
  faHourglassStart,
  faEdit,
  faStopCircle,
  faTasks,
  faBellSlash,
  faPauseCircle,
  faQuestionCircle,
  faSadCry,
  faInfinity,
  faCubes,
  faExchangeAlt,
  faEye,
  faSubway,
  faLock,
  faPlus,
  faThumbsUp,
  faThumbsDown,
  faHandRock,
  faFistRaised,
  faSearch,
  faCapsules,
  faSignInAlt,
  faSignOutAlt,
  faMoneyBillAlt,
  faHandHoldingUsd,
  faUserSlash,
  faHistory,
  faMusic,
  faTools,
  faPaperPlane,
  faUsers
} from "@fortawesome/free-solid-svg-icons";
import {
  faFacebookMessenger,
  faWhatsapp,
  faFacebook,
  faTwitter,
  faGithub,
  faGoogle
} from "@fortawesome/free-brands-svg-icons";

export const Icons = {
  faBolt,
  faMobileAlt,
  faTrain,
  faGasPump,
  faCalculator,
  faDivide,
  faStopwatch,
  faDrumstickBite,
  faSave,
  faCopy,
  faBrain,
  faMehRollingEyes,
  faFacebookMessenger,
  faWhatsapp,
  faEnvelopeSquare,
  faSms,
  faPhoneAlt,
  faHome,
  faArrowLeft,
  faArrowUp,
  faArrowDown,
  faStar,
  faHorse,
  faBox,
  faFacebook,
  faGithub,
  faFlag,
  faEllipsisH,
  faIdCard,
  faPercentage,
  faUserFriends,
  faSun,
  faPercent,
  faArrowCircleLeft,
  faEgg,
  faClock,
  faMapMarkerAlt,
  faLaughWink,
  faBusAlt,
  faTrashAlt,
  faPause,
  faPlay,
  faHourglassStart,
  faEdit,
  faStopCircle,
  faTasks,
  faBellSlash,
  faPauseCircle,
  faQuestionCircle,
  faSadCry,
  faInfinity,
  faCubes,
  faExchangeAlt,
  faEye,
  faSubway,
  faLock,
  faPlus,
  faThumbsUp,
  faThumbsDown,
  faHandRock,
  faFistRaised,
  faGoogle,
  faSearch,
  faCapsules,
  faSignInAlt,
  faSignOutAlt,
  faMoneyBillAlt,
  faHandHoldingUsd,
  faUserSlash,
  faHistory,
  faMusic,
  faTools,
  faPaperPlane,
  faUsers,
  faTwitter
};

export let camelCased = str =>
  str.replace(/-([a-z])/g, function(g) {
    return g[1].toUpperCase();
  });

export let reverseCamelCased = str =>
  str
    .replace(/^fa/gi, "")
    .replace(/.[A-Z]/g, function(g) {
      return `${g[0]}-${g[1].toLowerCase()}`;
    })
    .replace(/[A-Z]/g, function(g) {
      return g.toLowerCase();
    });

export function Icon(props) {
  if (!props.icon) return <></>;
  const IconName = camelCased("fa-" + props.icon);
  if (!Icons[IconName]) {
    console.error("couldn't find icon: ", IconName);
    return;
  }
  let Size = props.size || "lg";
  Size = props.sm ? "sm" : props.lg ? "lg" : Size;
  return (
    <FontAwesomeIcon
      icon={Icons[IconName]}
      size={Size}
      className={props.className}
      style={props.style}
      onClick={props.onClick}
    />
  );
}
