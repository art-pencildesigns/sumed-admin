import React from "react";
import { Icon } from "./Icon";

export default function Button(props) {
  let { children, icon, breakLine, ...otherProps } = props;
  return (
    <>
      {breakLine && (
        <span className="d-none d-sm-block">
          &nbsp; <br />
        </span>
      )}
      <button {...otherProps}>
        <Icon icon={icon} sm /> &nbsp; {children}
      </button>
    </>
  );
}
