import React from "react";

export default class Input extends React.Component {
  render() {
    let {
      reff,
      append,
      prepend,
      title,
      type,
      placeholder,
      value,
      help,
      label,
      id,
      subLabel,
      children,
      smallLabel,
      hideInput,
      sm,
      optional,
      parentClass,
      lableClassName,
      ...props
    } = this.props;
    const inputGroupStyle = [
      "input-group ",
      this.props.sm && "input-group-sm"
    ].join(" ");
    return (
      <div
        className={
          "form-group removeMarginBottom " + parentClass ? parentClass : ""
        }
      >
        {label && (
          <label htmlFor={id} className={lableClassName}>
            <b>{label}</b>
            {optional && <small> (Optional)</small>} <br />
            {subLabel && <small>{subLabel}</small>}
          </label>
        )}
        {smallLabel && (
          <small style={{ color: "#7b8a8b" }}>
            <b> {smallLabel}</b>
          </small>
        )}
        <div className={inputGroupStyle}>
          <div className="input-group-prepend">
            {(prepend || title) && (
              <span className="input-group-text">{prepend || title}</span>
            )}
          </div>
          {!hideInput && (
            <input
              type={type || "number"}
              className="form-control"
              placeholder={
                placeholder || title || label || smallLabel || "أدخل رقم"
              }
              value={value}
              id={id}
              ref={reff}
              {...props}
            />
          )}
          {children}
          {append && (
            <div className="input-group-append">
              {typeof append == "string" ? (
                <span className="input-group-text">{append}</span>
              ) : (
                  append
                )}
            </div>
          )}
        </div>
        {help && <small className="form-text text-danger">{help}</small>}
      </div>
    );
  }
}
