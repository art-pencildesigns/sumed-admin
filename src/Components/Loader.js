import React from "react";

export default function Loader(props) {
  let { children, loaded, error, errorMsg, ...otherProps } = props;
  if (error) {
    return errorMsg;
  } else if (loaded) {
    return <>{children}</>;
  } else {
    return (
      <div className="text-center">
        <div className="spinner-border text-primary" role="status">
          <span className="sr-only">Loading...</span>
        </div>
      </div>
    );
  }
}
