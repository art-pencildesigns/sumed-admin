import React from "react";
import { Icon } from "./Icon";

export default function Link(props) {
  return (
    <a href={props.href} target="_blank" {...props}>
      <Icon icon={props.icon} /> &nbsp;{props.title || props.children}
    </a>
  );
}
