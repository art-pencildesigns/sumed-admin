import React from "react";
// import { Helmet } from "react-helmet";

import AppLink from "./AppLink";
import { Icon } from "./Icon";
import Config from "../Config";
import { getUsedHowManyTimes } from "../Config/LocalStorage";
import { kFormatter } from "../Helpers/general";
import Button from "./Button";

function centeredPopup(url, winName) {
  let w = 800,
    h = 600,
    screen = window.screen,
    LeftPosition = screen.width ? (screen.width - w) / 2 : 0,
    TopPosition = screen.height ? (screen.height - h) / 2 : 0,
    settings = `menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600,top=${TopPosition},left=${LeftPosition}`,
    popupWindow = window.open(url, winName, settings);
}

export default function IntroductionBox(props) {
  let { icon, title, children, appName, desc } = props;
  let app;
  if (appName) {
    appName = appName.split(".");
    app = Config.MyApps[appName[0]][appName[1]];
    icon = app.icon;
    title = app.title;
    desc = app.desc;
  }

  let usedHowManyTimes,
    ThisAppUsedHowManyTimes = 0;
  if (props.usedHowManyTimes) {
    ThisAppUsedHowManyTimes = props.usedHowManyTimes[appName[1]] || 0;
  } else {
    usedHowManyTimes = getUsedHowManyTimes();
    ThisAppUsedHowManyTimes = usedHowManyTimes ? usedHowManyTimes[appName[1]] : 0;
  }

  ThisAppUsedHowManyTimes = kFormatter(ThisAppUsedHowManyTimes) || 0;

  let shared_data = {
    prepared_title: app.title,
    prepared_text: app.desc,
    prepared_url: window.location.href.replace("masry-helper/", `masry-helper/?customURI=${app.link}`),
    prepared_img: "img/icons/icon-144x144.png"
  };
  // console.log(shared_data);

  return (
    <div className="m-2 text-left">
      <AppLink className="btn btn-outline-secondary m-2" link="" title="الرجوع" icon="arrow-circle-left" />

      <div
        className="jumbotron bg-primary text-white hand IntroductionBox"
        data-toggle="collapse"
        data-target="#IntroductionBox"
        aria-expanded="false"
        aria-controls="IntroductionBox"
      >
        <h3 className="text-right " style={{ fontSize: "4vh" }}>
          <Icon icon={icon} size="sm" />
          &nbsp;
          {title}
          <small
            className="float-left badge badge-danger"
            title={`هذا البرنامج إستخدم ${ThisAppUsedHowManyTimes} مرة`}
            id="usedHowManyTimes"
            style={{ fontSize: "1.3rem" }}
          >
            {ThisAppUsedHowManyTimes}
          </small>
        </h3>
        <div className="lead text-right arabic2Bold collapse multi-collapse" id="IntroductionBox">
          <hr className="my-1" />
          {desc} {children}
        </div>
      </div>

      <div className="col-12 text-center mb-3">
        <Button
          icon="copy"
          className="btn btn-dark col-12 col-sm-4"
          data-clipboard-text={shared_data.prepared_url}
          onClick={evt => window.notify({ type: "success", title: "تم النسخ" })}
        >
          أنسخ
        </Button>

        <Button
          icon="facebook"
          className="btn social-facebook text-white col-12 col-sm-4"
          onClick={evt => {
            const link = `http://www.facebook.com/sharer.php?s=100&p[url]=${shared_data.prepared_url}&p[images][0]=${shared_data.prepared_img}&p[title]=${shared_data.prepared_title}&p[summary]=${shared_data.prepared_text}`;
            centeredPopup(link, "facebook");
          }}
        >
          شارك على Facebook
        </Button>
        <Button
          icon="twitter"
          className="btn social-twitter text-white col-12 col-sm-4"
          onClick={evt => {
            const link = `https://twitter.com/intent/tweet?url=${shared_data.prepared_url}&text=${shared_data.prepared_title}&via=${shared_data.twitter_via}&hashtags=${shared_data.tags}&related=${shared_data.twitter_related}`;
            centeredPopup(link, "twitter");
          }}
        >
          شارك على Twitter
        </Button>
      </div>
    </div>
  );
}
