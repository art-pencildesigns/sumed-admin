import React from "react";
export default function BigBlock(props) {
  return (
    <div className="col-sm-12 text-center BigBlock">
      <div className="card p-2">
        <h1 className="display-1 lead text-secondary">{props.children}</h1>
        {props.countDown && (
          <h3 className="display-3 lead text-danger">{props.countDown}</h3>
        )}
      </div>
    </div>
  );
}
