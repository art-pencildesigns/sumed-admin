import React from "react";
import { Modal } from "./Components";

export default props => {
    let { id, msg, okTxt = "submit", confirmed, canceled = _ => { } } = props;
    return (
        <Modal id={id} md>
            < div className="modal-body p-5" >
                <img
                    src="images/correct.svg"
                    width="180"
                    className="mr-auto ml-auto d-block pt-3"
                />
                <h3 className="font-weight-bold text-center pt-5 pb-3">
                    <b>Confirm</b>
                </h3>
                <div className="text-center">
                    <h6 className="font-weight-bold text-muted">{msg}</h6>
                </div>

                <div className="float-right mt-4">
                    <button
                        onClick={evt => {
                            canceled(evt);
                            window.$(`#${id}`).modal("hide");
                        }}
                        className="btn btn-default p-3 btn-round mr-5 text-muted"
                    >
                        Cancel
                </button>
                    <button
                        onClick={evt => {
                            confirmed(evt);
                            window.$(`#${id}`).modal("hide");
                        }}
                        className="btn btnContinue p-3"
                    > {okTxt} <span className="fa fa-thumbs-up ml-2"></span>
                    </button>
                </div>

            </div >
        </Modal >
    );
};
