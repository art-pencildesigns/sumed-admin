import React, { Component } from 'react';
import { HashRouter as Router, Route } from 'react-router-dom';

import MainPage from './Pages/MainPage';
import Committies from './Pages/Committies/Committies';
import CommittySummary from './Pages/Committies/CommittySummary';
import PrintMembers from './Pages/Committies/PrintMembers';
import Meetings from './Pages/Committies/Meetings';
import Suppliers from './Pages/Committies/Suppliers';
import Members from './Pages/Committies/Members';
import ShowMeeting from './Pages/Committies/ShowMeeting';

import Lists from './Pages/Lists/Lists';
import List from './Pages/Lists/List';

import Users from './Pages/Users/Users';

import Roles from './Pages/Roles/Roles';
import AdminLogs from './Pages/AdminLogs/AdminLogs';

import Requests from './Pages/Requests/Requests';
import SuppliersList from './Pages/Suppliers/SuppliersList';
import SuppliersShow from './Pages/Suppliers/SuppliersShow';
import Login from './Pages/Custom/Login';

import Activities from './Pages/Activities/Activities';
import ActivitiesShow from './Pages/Activities/ActivitiesShow';
import ActivityCreate from './Pages/Activities/ActivityCreate';

import Dashboard from './Pages/Dashboard/Dashboard';
import Search from './Pages/Search/Search';
import Emails from './Pages/Emails/Emails';
import EmailShow from './Pages/Emails/EmailShow';
import SupplierShow from './Pages/Suppliers/SuppliersShow';
import { Query } from './Helpers/Constants';
import TopNavBar from './Pages/TopNavbar';
import SideNav from './Pages/SideNav';
import SupplierMeetingResult from './Pages/Committies/SupplierMeetingResult';
import { RenderDeleteModal } from './Helpers/ReadyModals';

export default class app extends Component {
    state = {
        permissions: [],
        pathname: 'none',
        requestsCount: 0
    };

    updateUserData() {
        console.log('updating user data');
        Query('me')
            .then(res => {
                // const pathname = this.props.location.pathname;
                let pathname = window.location.hash.split('/')[1];
                // console.log({ props: this.props });
                const userProfile = res.data.data;
                this.setState({
                    pathname,
                    permissions: userProfile.roleFunctions,
                    requestsCount: userProfile.requestsCount
                });
            })
            .catch(error => {
                window.location.hash = '';
            });
    }

    componentWillMount() {
        if (localStorage.getItem('access_token')) {
            this.updateUserData();
        } else {
            window.location.hash = '';
        }
    }

    render() {
        let { pathname, permissions, requestsCount } = this.state;
        let searchWord = '';
        if (pathname == 'search') searchWord = window.location.hash.split('/')[2];

        if (pathname == '' || pathname == 'none') {
            return (
                <Router>
                    <Route exact path="/" component={_ => <Login />} />
                </Router>
            );
        }

        return (
            <Router>
                <div>
                    <TopNavBar location={pathname} querySearchWord={searchWord} requestsCount={requestsCount} />
                    {RenderDeleteModal()}
                    <div className="container-fluid">
                        <div className="row">
                            <SideNav location={pathname} />
                            <div className="col-lg-10 col-md-9 col-sm-12 col-12 sideContent" id="RightContent">
                                <div className="loaderParent">
                                    <div className="loader">Loading...</div>
                                </div>

                                <Route exact path="/admin-logs" component={props => <MainPage MainState={this.state} {...props} Comp={<AdminLogs {...props} />} />} />
                                <Route exact path="/" component={props => <MainPage MainState={this.state} {...props} Comp={<Dashboard />} />} />
                                <Route exact path="/dashboard" component={props => <MainPage MainState={this.state} {...props} Comp={<Dashboard />} />} />
                                <Route exact path="/search/:searchWord" component={props => <MainPage MainState={this.state} {...props} Comp={<Search {...props} />} />} />
                                <Route exact path="/activities" component={props => <MainPage MainState={this.state} {...props} Comp={<Activities />} />} />
                                <Route exact path="/activities/:id" component={props => <MainPage MainState={this.state} {...props} Comp={<ActivitiesShow {...props} />} />} />
                                <Route exact path="/activity-create" component={props => <MainPage MainState={this.state} {...props} Comp={<ActivityCreate />} />} />
                                <Route exact path="/committees" component={props => <MainPage MainState={this.state} {...props} Comp={<Committies />} />} />
                                <Route exact path="/committees/:id/summary" component={props => <MainPage MainState={this.state} {...props} Comp={<CommittySummary {...props} />} />} />
                                <Route exact path="/committees/:committeeId/print-members/:meetingId" component={props => <MainPage MainState={this.state} {...props} Comp={<PrintMembers {...props} />} />} />
                                <Route exact path="/committees/:id/meetings/:meetingID" component={props => <MainPage MainState={this.state} {...props} Comp={<ShowMeeting {...props} />} />} />
                                <Route exact path="/committees/:id/meetings/:meetingID/supplier/:supplierID" component={props => <MainPage MainState={this.state} {...props} Comp={<SupplierMeetingResult {...props} />} />} />
                                <Route exact path="/committees/:id/meetings" component={props => <MainPage MainState={this.state} {...props} Comp={<Meetings {...props} />} />} />
                                <Route path="/committees/:id/suppliers" component={props => <MainPage MainState={this.state} {...props} Comp={<Suppliers {...props} />} />} />
                                <Route path="/committees/:id/members" component={props => <MainPage MainState={this.state} {...props} Comp={<Members {...props} />} />} />
                                <Route exact path="/lists" component={props => <MainPage MainState={this.state} {...props} Comp={<Lists {...props} />} />} />
                                <Route path="/list/:id" component={props => <MainPage MainState={this.state} {...props} Comp={<List {...props} />} />} />
                                <Route path="/users" component={props => <MainPage MainState={this.state} {...props} Comp={<Users {...props} />} />} />
                                <Route exact path="/requests" component={props => <MainPage MainState={this.state} {...props} Comp={<Requests requestsCount={requestsCount} {...props} />} />} />
                                <Route path="/roles" component={props => <MainPage MainState={this.state} {...props} Comp={<Roles {...props} />} />} />
                                <Route path="/requests/:id" component={props => <MainPage MainState={this.state} {...props} Comp={<SupplierShow {...props} pageType="request" />} />} />
                                <Route path="/suppliers/:id" component={props => <MainPage MainState={this.state} {...props} Comp={<SuppliersShow {...props} pageType="supplier" />} />} />
                                <Route exact path="/suppliers-list/:status?" component={props => <MainPage MainState={this.state} {...props} Comp={<SuppliersList {...props} />} />} />
                                <Route exact path="/emails/:id" component={props => <MainPage MainState={this.state} {...props} Comp={<EmailShow {...props} />} />} />
                                <Route exact path="/emails" component={props => <MainPage MainState={this.state} {...props} Comp={<Emails />} />} />
                            </div>
                        </div>
                    </div>
                </div>
            </Router>
        );
    }
}
