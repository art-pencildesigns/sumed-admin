import Axios from 'axios';

let APIroute;
// APIroute = 'http://sumed.com/admin-api';
// APIroute = "https://pencil-designs.com/sumed/public/admin-api";
// APIroute = "http://pencilproduction.com/sumedAdmin/backend/public/admin-api";
APIroute = 'http://ec2-3-23-130-173.us-east-2.compute.amazonaws.com/sumed/public/admin-api';

// export let FrontendRoute = 'http://localhost:3000/';
// export let FrontendRoute = "http://pencilproduction.com/sumedAdmin/front/";
export let FrontendRoute = 'http://ec2-3-23-130-173.us-east-2.compute.amazonaws.com/sumed-admin-frontend/build/';

// export let BackendRoute = 'http://sumed.com/';
// export let BackendRoute = 'http://pencilproduction.com/sumedAdmin/backend/public/';
export let BackendRoute = 'http://ec2-3-23-130-173.us-east-2.compute.amazonaws.com/sumed/public/';
export function Query(url, method = 'get', params = {}) {
    let token = 'Bearer ' + localStorage.getItem('access_token');

    url = APIroute + '/' + url;
    let axiosInstance = Axios({
        method,
        url,
        data: params,
        headers: {
            Authorization: token
            // "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9sb2NhbGhvc3Q6ODA4MFwvc3VtZWRcL3B1YmxpY1wvYWRtaW4tYXBpXC9sb2dpbiIsImlhdCI6MTU3Mjc4MzY4OCwibmJmIjoxNTcyNzgzNjg4LCJqdGkiOiJQYWFkeEVjblFORjZSWDFsIiwic3ViIjo1LCJwcnYiOiJkZjg4M2RiOTdiZDA1ZWY4ZmY4NTA4MmQ2ODZjNDVlODMyZTU5M2E5In0.5EYmEZdsGTH0gFDdygwy98yHmG1OUTz8HqrYl-60xYA"
        }
    });

    return axiosInstance;
}

export const Avatar = './images/avatar.png';

// git clone https://art-pencildesigns@bitbucket.org/art-pencildesigns/sumed-admin-frontend-productionbuild.git sumed-admin
// sudo find /sumed-admin -type f -delete && sudo rm -r /sumed-admin
// http://ec2-3-23-130-173.us-east-2.compute.amazonaws.com/sumed/public/
// http://ec2-3-23-130-173.us-east-2.compute.amazonaws.com/sumed-admin/
