import { Query } from './Constants';

export function animateCSS(element, animationName, callback) {
    const node = document.querySelector(element);
    if (node) {
        node.classList.add('animated', animationName);
        function handleAnimationEnd() {
            if (typeof callback === 'function') callback();
            node.classList.remove('animated', animationName);
            node.removeEventListener('animationend', handleAnimationEnd);
        }
        node.addEventListener('animationend', handleAnimationEnd);
    } else {
        console.error("couldn't find element: " + element);
        if (typeof callback === 'function') callback();
    }
}

export function addClass(element, classes) {
    const node = document.querySelector(element);
    if (node) {
        node.classList.add(...classes);
    }
}
export function removeClass(element, classes) {
    const node = document.querySelector(element);
    if (node) {
        node.classList.remove(...classes);
    }
}

export function cloneObj(obj) {
    return Object.assign({}, obj);
}

export function generateRandomCode(length) {
    var result = '';
    var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for (var i = 0; i < length; i++) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
}

export function getRandomColor() {
    var letters = '0123456789ABCDEF';
    var color = '#';
    for (var i = 0; i < 6; i++) {
        color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
}

export function random_rgba(opacity) {
    var o = Math.round,
        r = Math.random,
        s = 255;
    return (
        'rgba(' +
        o(r() * s) +
        ',' +
        o(r() * s) +
        ',' +
        o(r() * s) +
        ',' +
        opacity +
        // r().toFixed(1) +
        ')'
    );
}

export function arrayUniq(arr) {
    let unique = [...new Set(arr)];
    return unique;
}

export function updateUserData() {
    Query('me').then(res => {
        const userProfile = res.data.data;
        localStorage.setItem('permissions', JSON.stringify(userProfile.roleFunctions));
        localStorage.setItem('requestsCount', JSON.stringify(userProfile.requestsCount));
    });
}

export function printFile(url) {
    let screen = window.screen;
    var winPrint = window.open(url, 'printing', 'width=' + screen.availWidth + ',height=' + screen.availHeight + 'left=0,top=0,toolbar=0,scrollbars=0,status=0');
    winPrint.focus();
    winPrint.print();
    return true;
}

export function PrintContent(content) {
    let screen = window.screen;
    let winPrint = window.open('', 'printing', 'width=' + screen.availWidth + ',height=' + screen.availHeight + 'left=0,top=0,toolbar=0,scrollbars=0,status=0');

    winPrint.document.write('<html><head><title>Print</title>');
    winPrint.document.write(`
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/css/bootstrap.min.css" />
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.10.0-11/css/all.min.css" />
      <link rel="stylesheet" href="css/style.css" />
      <link rel="stylesheet" href="css/styles2.css" />
      <link href="https://fonts.googleapis.com/css?family=Roboto:400,700,900&display=swap" rel="stylesheet" />
      `);
    winPrint.document.write('</head><body >');
    winPrint.document.write(content);
    winPrint.document.write('</body></html>');

    winPrint.document.close(); // necessary for IE >= 10
    winPrint.focus(); // necessary for IE >= 10*/
    winPrint.print();
    setTimeout(() => {
        winPrint.close();
    }, 2000);
    return true;
}

export function supplierStatusClass(status) {
    let statusClass = '';
    if (status == 'filtered') statusClass += 'text-info';
    else if (status == 'approved') statusClass += 'text-success';
    else if (status == 'rejected') statusClass += 'text-danger';
    else if (status == 'frozen') statusClass += 'text-primary';
    else if (status == 'not submited') statusClass += 'text-muted';
    else if (status == 'pending approval') statusClass += 'text-warning';
    return statusClass;
}

export function supplierStatusNamingFix(status) {
    if (status == 'filtered') status = 'Revised';
    else if (status == 'not submited') status = 'Not Submitted';
    return status;
}

export function HandleErrors(error) {
    window.$('.loaderParent').hide();
    console.error({ error });
    console.error('request failed: ', {
        error,
        response: error.response,
        msg: error.response.data.message
    });
    window.alert(error.response.data.message);
}

export function Naming(str_or_arr, value, name) {
    let newArray = [];
    if (typeof str_or_arr == 'string') newArray = [str_or_arr];
    else newArray = [...str_or_arr];

    for (let itiration in value) {
        let index = newArray.indexOf(value[itiration]);
        if (index !== -1) newArray[index] = name[itiration];
    }
    return newArray;
}
