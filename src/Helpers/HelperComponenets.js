import React, { Component } from 'react';

export function Not(txt) {
    return <span className="text-danger"> {txt || 'Not Finished'} </span>;
}

export function getPermission(name) {
    // console.log(JSON.parse(localStorage.getItem('permissions')));
    return JSON.parse(localStorage.getItem('permissions'))[name].edit;
}

export function checkPermission(name) {
    let allow = JSON.parse(localStorage.getItem('permissions'))[name].edit;
    if (allow) return true;
    window.alert("You Don't have permission for Modifying This Page content.");
    return false;
}

export function isSuperAdmin() {
    let role_id = localStorage.getItem('role_id');
    if (role_id == 1) return true;
    window.alert('Only superAdmins are allowed to take this action. Sorry, you are not allowed.');
    return false;
}
