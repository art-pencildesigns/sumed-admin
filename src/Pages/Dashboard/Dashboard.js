import React, { Component } from 'react';
import Header from '../Header';
import { Query } from '../../Helpers/Constants';
import { random_rgba, animateCSS } from '../../Helpers/General';
import { Link } from 'react-router-dom';

export default class DashboardPage extends Component {
    state = {
        dashBoardData: {
            pending_requests: 0,
            filtered_requests: 0,
            total_suppliers: 0,
            last_approved_suppliers: [],
            upcoming_meetings: [],
            meetings: []
        }
    };

    async componentDidMount() {
        // animateCSS("#RightContent", "bounceOutRight", _ => {
        //   animateCSS("#RightContent", "bounceInRight");
        // });
        console.log('dddddddddddd');
        let dashBoardData = await Query('dashboard-info');
        dashBoardData = dashBoardData.data.data;
        console.log({ dashBoardData });
        this.setState({ dashBoardData });
        let data = {
            labels: [],
            datasets: [
                {
                    label: '# of Votes',
                    data: [],
                    backgroundColor: ['rgba(85, 216, 254, .7)', 'rgba(255, 131, 115, .7)', 'rgba(255, 218, 131, .7)', 'rgba(163, 160, 251, .7)', 'rgba(255, 87, 34, .7)', 'rgba(156, 39, 176, .7)'],
                    borderColor: ['rgba(85, 216, 254, 1)', 'rgba(255, 131, 115, 1)', 'rgba(255, 218, 131, 1)', 'rgba(163, 160, 251, 1)', 'rgba(255, 87, 34, 1)', 'rgba(156, 39, 176, 1)'],
                    borderWidth: 2
                }
            ]
        };

        window.$('#Dashboard').show(1000);

        setTimeout(() => {
            var elm = document.getElementById('myChart');
            if (!elm) return;
            let ctx = elm.getContext('2d');
            for (let meeting of dashBoardData.meetings) {
                let color = random_rgba(0.5);
                console.log({ color });
                data.labels.push(meeting.topic + ' - ' + meeting.suppliers_count + ' Suppliers');
                // data.datasets[0].data.push(meeting.suppliers_count);
                data.datasets[0].data.push(5);
                // data.datasets[0].backgroundColor.push(color);
                // data.datasets[0].borderColor.push("rgba(0,0,0,.1)");
            }
            console.log(data);
            // let data = {
            //   labels: [
            //     "Meeting 1 - 10 Suppliers",
            //     "Meeting 2 - 4 Suppliers",
            //     "Meeting 3 - 40 Suppliers",
            //     "Meeting 4 - 50 Suppliers"
            //   ],
            //   datasets: [
            //     {
            //       label: "# of Votes",
            //       data: [12, 19, 3, 44],
            //       backgroundColor: [
            //         "rgba(255, 99, 132, 0.7)",
            //         "rgba(54, 162, 235, 0.7)",
            //         "rgba(255, 206, 86, 0.7)",
            //         "rgba(200, 180, 66, 0.7)"
            //       ],
            //       borderColor: [
            //         "rgba(255, 99, 132, 1)",
            //         "rgba(54, 162, 235, 1)",
            //         "rgba(255, 206, 86, 1)",
            //         "rgba(200,  180, 66, 1)"
            //       ],
            //       borderWidth: 1
            //     }
            //   ]
            // };
            var myDoughnutChart = new window.Chart(ctx, {
                type: 'doughnut',
                data: data
            });
        }, 1000);
    }

    render() {
        let { dashBoardData } = this.state;
        return (
            <div className="p-0 m-3 mt-5" id="Dashboard">
                <Header title="Dashboard" icon="home" />
                <div className="row mt-5">
                    <div className="col-4">
                        <div className="shadowBox bg-white radius p-4 bluish">
                            <h1>{dashBoardData.pending_requests}</h1>
                            <p className="font-size2">Pending Requests</p>
                            <Link className="text-muted mt-4" to="/requests">
                                View All
                            </Link>
                        </div>
                    </div>
                    <div className="col-4">
                        <div className="shadowBox bg-white radius p-4 bluish">
                            <h1>{dashBoardData.filtered_requests}</h1>
                            <p className="font-size2">Revised Requests</p>
                            <Link className="text-muted mt-4" to="/suppliers-list/filtered">
                                View All
                            </Link>
                        </div>
                    </div>
                    <div className="col-4">
                        <div className="shadowBox bg-white radius p-4 bluish">
                            <h1>{dashBoardData.total_suppliers}</h1>
                            <p className="font-size2">Total Suppliers</p>
                            <Link className="text-muted mt-4" to="/suppliers-list">
                                View All
                            </Link>
                        </div>
                    </div>
                </div>
                <div className="row">
                    <div className="col-12 mt-5">
                        <p className="mb-4 bluish">Latest Approved Suppliers</p>
                    </div>

                    <div className="col-8">
                        <div className="shadowBox bg-white radius p-0 bluish">
                            {dashBoardData.last_approved_suppliers.length > 0 ? (
                                <table className="table" id="Table">
                                    <tbody>
                                        {dashBoardData.last_approved_suppliers.map((supplier, index) => (
                                            <tr key={index}>
                                                <td>{supplier.username}</td>
                                                <td>
                                                    {supplier.ActivityNames &&
                                                        supplier.ActivityNames.map((activity, index) => {
                                                            return (
                                                                <div>
                                                                    <span> {activity.join(' > ')} </span> <br />{' '}
                                                                </div>
                                                            );
                                                        })}
                                                    {/* // supplier.ActivityNames.join(" <br /> ")} */}
                                                </td>
                                            </tr>
                                        ))}
                                    </tbody>
                                </table>
                            ) : (
                                <h4 className="p-5">There are no results right now</h4>
                            )}
                        </div>
                        <p className="mt-5 bluish">Upcoming Meetings Today</p>
                        <div className="shadowBox bg-white radius p-4 bluish">
                            {dashBoardData.upcoming_meetings.filter(item => item.isValid).length > 0 ? (
                                dashBoardData.upcoming_meetings
                                    .filter(item => item.isValid)
                                    .map((meeting, index) => {
                                        return (
                                            <React.Fragment key={index}>
                                                <div className="row">
                                                    <div className="col-6">
                                                        <h1 className="mb-0">{meeting.start_time_processed}</h1>
                                                        <p className="font-size2">Start Time</p>
                                                    </div>
                                                    <div className="col-6">
                                                        <span className="font-size2 dashBoardMeetingTopic">{meeting.topic}</span>
                                                    </div>
                                                </div>
                                                {index + 1 != dashBoardData.upcoming_meetings.length && <hr />}
                                            </React.Fragment>
                                        );
                                    })
                            ) : (
                                <h4 className="p-3 text-center">There are no meetings today</h4>
                            )}
                        </div>
                    </div>

                    <div className="col-4">
                        <div className="shadowBox bg-white radius p-4">
                            <h1 className="text-center">Open Meetings</h1>
                            <h5 className="text-center">{dashBoardData.suppliers_count_in_meetings} Suppliers</h5>
                            <div className="row" id="DashboardChart">
                                <canvas id="myChart" width="400" height="400"></canvas>

                                {/* <ul>
                  <li>Adele</li>
                  <li>Agnes</li>
                  <li>Billy</li>
                  <li>Bob</li>
                </ul> */}
                            </div>
                        </div>
                    </div>

                    <div className="col-8 mt-5"></div>
                </div>
            </div>
        );
    }
}
