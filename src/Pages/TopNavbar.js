import React, { Component } from "react";
import { Link } from "react-router-dom";
import { Query } from "../Helpers/Constants";
import Img from "../Components/Img";
import history from "../history";


export default class TopNavBar extends Component {
  state = {
    searchWord: ""
  };

  logout(evt) {
    if (evt) evt.preventDefault();
    Query("logout");
    window.localStorage.clear();
    window.location.hash = "";
    window.location.reload();
  }

  searchText(evt) {
    // console.log(this.props.history);
    evt.preventDefault();
    history.push("/search/" + this.state.searchWord)
    // window.location.hash = "search/" + this.state.searchWord;
    window.location.reload();
  }

  componentDidMount() {
    let { querySearchWord } = this.props;
    let name = localStorage.getItem("name");
    let image = localStorage.getItem("image");
    if (!name) this.logout();
    this.setState({ name, image });
  }

  render() {
    let props = this.props;
    let { name, image, searchWord } = this.state;
    return (
      <nav
        className="navbar navbar-expand-lg navbar-light shadow"
        id="TopNavBar"
      >

        <Link
          to="/dashboard"
          className="navbar-brand ml-4 mr-5"
          onClick={evt => {
            window.$(".nav-link").removeClass("active");
          }}
        >
          <img src="images/sumed.png" />
        </Link>

        <button
          className="navbar-toggler"
          type="button"
          data-toggle="collapse"
          data-target="#topNavBarContent"
          aria-controls="topNavBarContent"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span className="navbar-toggler-icon"></span>
        </button>

        <div className="collapse navbar-collapse" id="topNavBarContent">
          <ul className="navbar-nav mx-auto">
            <li className="nav-item mr-3">
              <Link
                to="/requests"
                className={
                  "nav-link px-4 " +
                  (/requests/.test(props.location) ? "active" : "")
                }
                onClick={evt => {
                  window.$(".nav-link").removeClass("active");
                  window.$(evt.currentTarget).addClass("active");
                }}
              >
                <i className="fa fa-bell"></i>&nbsp;
                Requests&nbsp;
                <span className="badge badgehead">{props.requestsCount}</span>
              </Link>
            </li>
            {/* <li className="nav-item mr-3">
            <a className="nav-link" href="#">
              Reports
            </a>
          </li> */}
            <li className="nav-item">
              <Link
                to="/roles"
                className={
                  "nav-link px-4 " +
                  (/role/.test(props.location) ? "active" : "")
                }
                onClick={evt => {
                  window.$(".nav-link").removeClass("active");
                  window.$(evt.currentTarget).addClass("active");
                }}
              >
                <i className="fa fa-cog"></i>&nbsp;
                Manage
              </Link>
            </li>
          </ul>
          <ul className="navbar-nav ml-auto" dir="rtl">
            <li className="nav-item ml-2 dropdown show">
              <a
                className="nav-link dropdown-toggle"
                href="#"
                role="button"
                // id="dropdownMenuLink"
                data-toggle="dropdown"
                aria-haspopup="true"
                aria-expanded="false"
              >
                {name} <Img src={image} className="imageNavbar" />
              </a>

              <div className="dropdown-menu" aria-labelledby="dropdownMenuLink">
                <a className="dropdown-item" href="#" onClick={this.logout}>
                  Logout
                </a>
              </div>

              {/* <a href="#" className="nav-link">
              {name} <img src={image} className="imageNavbar" />
            </a> */}
            </li>
            <li className="nav-item mt-1 ml-2">
              <Link
                className="nav-link"
                to="/admin-logs"
              >
                {/* <img className="msg" src="images/Ellipse .png" /> */}
                {/* <img src="images/message.png" /> */}
                <i className="fa fa-users-cog fa-2x"></i>
              </Link>
            </li>
            {/* <li className="nav-item mt-1 ml-2">
            <a className="nav-link" href="#">
              <img src="images/setting.png" />
            </a>
          </li> */}
            <li className="nav-item mt-2 mr-3">
              <form
                className="input-group mb-3"
                onSubmit={this.searchText.bind(this)}
              >
                <input
                  type="text"
                  className="form-control formSearch border-top-0 border-right-0 border-left-0"
                  placeholder="Type to search"
                  dir="ltr"
                  value={searchWord}
                  onChange={evt => {
                    this.setState({ searchWord: evt.target.value });
                  }}
                />
                <div className="input-group-prepend">
                  <span
                    className="input-group-text boxSearch border-top-0 border-right-0 border-left-0"
                    id="basic-addon1"
                  >
                    <i className="fa fa-search"></i>
                  </span>
                </div>
                <button type="submit" className="d-none" />
              </form>
            </li>
          </ul>
        </div>
      </nav>
    );
  }
}
