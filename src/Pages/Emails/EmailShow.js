import React, { Component } from "react";
import {
    animateCSS,
    removeClass,
    addClass,
    cloneObj,
    printFile,
    HandleErrors
} from "../../Helpers/General";
import { Modal, Input } from "../../Components";
import { TableView, GenerateTable1, GenerateTable2 } from "../TableView";
import { Link } from "react-router-dom";
import Header from "../Header";
import { Query } from "../../Helpers/Constants";
import { checkPermission } from "../../Helpers/HelperComponenets";

export default class EmailsShow extends Component {
    state = {
        email: {
            name: "",
            content: "",
            content_ar: ""
        },
        newObj: {
            id: false,
            name: "",
            description: "",
            step: 1,
            category: ""
        },
        selectFromArray: [],
        filteringText: "",
        uploadedFileName: "",
        uploadingFile: false
    };

    resetNewObj() {
        let newObj = {
            id: false,
            name: "",
            description: "",
            step: 1,
            category: ""
        };
        this.setState({ newObj });
    }

    deleteRow = ({ id }) => evt => {
        evt.preventDefault();
        let promise = Query(`email-template/${id}`, "delete");
        promise.then(res => {
            let { list } = this.state;
            animateCSS(`#id-${id}`, "bounceOutLeft", _ => {
                list = list.filter(item => item.id != id);
                this.setState({ list });
            });
        }).catch(HandleErrors);
    };

    showEditRow = ({ listItem }) => evt => {
        evt.preventDefault();
        let { newObj } = this.state;
        newObj.id = listItem.id;
        newObj.name = listItem.name;
        newObj.category = listItem.category;
        newObj.deletable = listItem.deletable;
        newObj.content = listItem.content;
        newObj.lang = listItem.lang;
        this.setState({ newObj });
        window.$("#editModal").modal("show");
    };

    editRow = () => evt => {
        evt.preventDefault();
        //TODO: update the backend
        let { name, description, id } = this.state.newObj;
        let promise = Query(`committee/${id}`, "put", { name, description });
        promise
            .then(results => {
                this.refreshList();
                this.resetNewObj();
                window.$("#editModal").modal("hide");
            })
            .catch(error => {
                console.error("request failed: ", {
                    error,
                    response: error.response,
                    msg: error.response.data.message
                });
                window.alert(error.response.data.message);
            });
    };

    refreshList() {
        let id = this.props.match.params.id;
        let promise = Query(`email-template/${id}`).then(res => {
            let email = res.data.data;
            this.setState({ email });
        });
    }
    refreshSubList() {
        // let promise2 = Query("admin-members").then(res => {
        //   let selectFromArray = res.data.data;
        //   selectFromArray = GenerateTable2(selectFromArray);
        //   this.setState({ selectFromArray });
        // });
    }

    addRow = () => evt => {
        evt.preventDefault();
        let { newObj } = this.state;
        let { name, category } = newObj;
        let promise = Query("create-email-template", "post", {
            name,
            category: category,
            content: " ",
            deletable: 0,
            lang: "en"
        });
        promise
            .then(results => {
                // let id = results.data.data.id; //setting the committee id
                this.refreshList();
                this.resetNewObj();
                // this.refreshSubList();
                // this.setState({ newObj: { ...newObj, id, step: 2 } });
                window.$("#createModal").modal("hide");
            })
            .catch(HandleErrors);
    };

    errorHandling(error) {
        console.error("request failed: ", {
            error,
            response: error.response,
            msg: error.response.data.message
        });
        window.alert(error.response.data.message);
    }

    submitSelectingModal = params => evt => {
        evt.preventDefault();
        let { newObj, selectFromArray } = this.state;
        let admin_ids = selectFromArray
            .filter(item => item.checked)
            .map(item => {
                return item.id;
            });
        let promise = Query("crud-committee-member", "post", {
            committee_id: newObj.id,
            admin_ids,
            delete: false
        })
            .then(res => {
                this.refreshList();
                this.setState({ newObj: { ...newObj, step: 3 } });
            })
            .catch(HandleErrors);
        // window.$("#createModal").modal("hide");
    };

    updateSelectingModal = ({ selectItem }) => evt => {
        let { selectFromArray } = this.state;
        selectFromArray = selectFromArray.map(item => {
            if (item.id == selectItem.id) item.checked = !item.checked;
            return item;
        });
        this.setState({ selectFromArray });
    };

    async componentDidMount() {
        this.refreshList();
        // animateCSS("#RightContent", "bounceOutRight", _ => {
        //     animateCSS("#RightContent", "bounceInRight");
        // });
    }

    render() {
        let { email, uploadedFileName, uploadingFile } = this.state;
        if (!email.name) return <div></div>;
        return (
            <div className="p-2 m-3 mt-5">
                {this.renderCreationModal()}
                {this.renderEditModal()}
                <div className="row">
                    <Header
                        icon="envelope-square"
                        title={email.name}
                        subTitle={`(${email.category})`}
                        className=" "
                        back="Back to Emails"
                        to="/emails"
                    />
                </div>

                <h4 className="mt-4">Title</h4>
                <div className="bg-white p-3 mt-3 shadowBox radius">
                    <input placeholder="Title" className="form-control border-0" value={email.name} onChange={this.change("name")} />
                </div>

                <h4 className="mt-4">Email Content</h4>
                <div className="bg-white p-3 mt-3 shadowBox radius">
                    <textarea className="form-control border-0" rows="5" value={email.content} onChange={this.change("content")}></textarea>
                </div>

                <h4 className="mt-4">Content in Arabic</h4>
                <div className="bg-white p-3 mt-3 shadowBox radius">
                    <textarea className="form-control border-0" rows="5" value={email.content_ar} onChange={this.change("content_ar")}></textarea>
                </div>

                <h4 className="mt-4">Attached Documents</h4>
                <div className="">
                    <input type="file" accept=".pdf,.doc,.docs,.jpg,.png" className="d-none" id="hiddenInput" onChange={this.uploadFile()} />
                    <div className="shadowBox radius uploadBox" onClick={this.openUploadPanel()}>
                        <i className="fa fa-cloud-upload-alt"></i>
                    </div>
                    {uploadingFile && <div className="progress">
                        <div className="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" id="progressBar">
                            <span>{uploadedFileName}</span>
                        </div>
                    </div>}

                    <div className="shadowBox bg-white p-3 mt-2 radius">
                        {email.documents.map((file, index) => {
                            return <div className="row" key={index}>
                                <a className="col-10 text-dark" href={file.path} download={file.name} target="_blank">{file.name}</a>
                                <div className="col-2">
                                    <a onClick={this.deleteFile({ fileId: file.id })}><i className="fa fa-trash-alt mx-2 hand text-danger" /></a>
                                    <a className="text-muted ml-2 hand" onClick={evt => printFile(file.path)}><i className="fa fa-print" /></a>
                                    <a className="text-dark ml-2" href={file.path} download={file.name} target="_blank"><i className="fa fa-download fDownload" /></a>
                                </div>
                                <hr />
                            </div>
                        })}
                    </div>
                </div>

                <div className="mt-5">
                    <button className="btn btn-outline-suceess btnCreate mt-0 align-middle float-right"
                        onClick={this.saveEmail}> <i className="fa fa-save"></i> &nbsp; Save </button>
                </div>

                <br /><br /><br />
            </div>
        );
    }


    openUploadPanel = params => evt => {
        evt.preventDefault();
        window.$("#hiddenInput").click();
    }
    uploadFile = params => evt => {
        evt.preventDefault();
        let files = evt.target.files;
        for (var i = 0, file; file = files[i]; i++) {
            let ext = file.name.substr(file.name.lastIndexOf('.') + 1);
            if (!["pdf", "jpg", "png", "doc", "docx"].includes(ext.toLowerCase())) {
                window.alert("You can't upload files with these extension.");
                return;
            }
            let formData = new FormData();
            formData.append("names", file.name.replace(/ /g, "-"));
            formData.append("file", file);
            this.setState({ uploadedFileName: file.name, uploadingFile: true });
            Query("upload-file", "post", formData).then(res => {
                let { email } = this.state;
                let email_id = res.data.data[0].id;
                email.documents.push({ id: email_id });
                this.saveEmail();
                this.setState({ uploadingFile: false });
            }).catch(HandleErrors);
        }
    }

    deleteFile = ({ fileId }) => evt => {
        evt.preventDefault();
        let { email } = this.state;
        email.documents = email.documents.filter(item => item.id != fileId)
        this.saveEmail();
    }



    saveEmail = evt => {
        if (evt) evt.preventDefault();
        if (!checkPermission("manage")) return;
        let { id, name, content, content_ar, deletable, category, lang, documents } = this.state.email;
        documents = documents.map(item => item.id)
        Query(`email-template/${id}`, "put", { name, content, content_ar, deletable, category, lang, documents }).then(res => {
            this.refreshList();
            window.alert("Saved");
        }).catch(HandleErrors)
    }

    change = params => evt => {
        evt.preventDefault();
        let value = evt.target.value;
        let { email } = this.state;
        let { name, content, content_ar } = email;
        switch (params) {
            case "name": name = value; break;
            case "content": content = value; break;
            case "content_ar": content_ar = value; break;
            default:
                console.error("something went wrong");
        }
        this.setState({
            email: { ...email, name, content, content_ar },
            // filteringText
        });
    };

    renderEditModal() {
        let { newObj } = this.state;
        return (
            <Modal id="editModal" onSubmit={this.editRow()}>
                <form onSubmit={this.editRow()}>
                    <h2 className="pb-4">Edit Email Template</h2>
                    <Input
                        label="Template Name"
                        type="text"
                        value={newObj.name}
                        onChange={this.change("name")}
                        className="form-control customModel"
                    />

                    <div className="form-group mt-4">
                        <label>
                            <b>Category</b>
                        </label>
                        <select
                            className="form-control customModel"
                            value={newObj.category}
                            onChange={this.change("category")}
                            required
                        >
                            <option disabled value="">
                                Select Case
            </option>
                            <option value="supplier_rejection">Supplier Rejection</option>
                            <option value="category">Category</option>
                            <option value="authentication">authentication</option>
                            <option value="supplier_acceptation">Supplier Acception</option>
                        </select>
                    </div>

                    <div className="modal-footer d-block px-5 border-top-0">
                        <button
                            type="button"
                            className="bg-white text-muted border-0"
                            data-dismiss="modal"
                        >
                            Cancel
            </button>
                        <button
                            type="button"
                            className="btn btnContinue float-right"
                            type="submit"
                        >
                            Save
              <i className="fa fa-arrow-circle-right ml-4"></i>
                        </button>
                    </div>
                </form>
            </Modal>
        );
    }

    renderCreateObj() {
        let { newObj } = this.state;
        return (
            <form onSubmit={this.addRow()} className="p-3">
                <h2 className="pb-4">New Email Template</h2>
                <Input
                    label="Template Name"
                    type="text"
                    value={newObj.name}
                    onChange={this.change("name")}
                    className="form-control customModel"
                    required
                />

                <div className="form-group mt-4">
                    <label>
                        <b>Category</b>
                    </label>
                    <select
                        className="form-control customModel"
                        value={newObj.category}
                        onChange={this.change("category")}
                        required
                    >
                        <option disabled value="">
                            Select Case
            </option>
                        <option value="supplier_rejection">Supplier Rejection</option>
                        <option value="category">Category</option>
                        <option value="supplier_acceptation">Supplier Acception</option>
                    </select>
                </div>

                <div className="modal-footer d-block px-5 border-top-0">
                    <button
                        type="button"
                        className="bg-white text-muted border-0"
                        data-dismiss="modal"
                    >
                        Cancel
          </button>
                    <button
                        type="button"
                        className="btn btnContinue float-right"
                        type="submit"
                    >
                        Create Email Template
            <i className="fa fa-arrow-circle-right ml-4"></i>
                    </button>
                </div>
            </form>
        );
    }

    renderSuccessMessage() {
        let numberOfSelected = this.state.selectFromArray.filter(
            item => item.checked
        );
        return (
            <div className="modal-body p-5">
                <img
                    src="images/correct.svg"
                    className="imgSuccess mr-auto ml-auto d-block pt-3"
                />
                <h4 className="font-weight-bold text-center pt-5 pb-3">
                    Meeting has been added successfully
        </h4>
                <p className="text-center">
                    <small className="font-weight-bold text-muted">
                        {numberOfSelected.length} Members in the Committee
          </small>
                </p>
            </div>
        );
    }

    renderCreationModal() {
        let { newObj } = this.state;
        return (
            <Modal id="createModal">
                {newObj.step == 1 && this.renderCreateObj()}
                {newObj.step == 2 && this.renderSuccessMessage()}
            </Modal>
        );
    }


    renderRedirectToLink(txt) {
        return <td>
            <Link to={`/email`} className="width100 capitilize">
                {txt}
            </Link>
        </td>
    }

    renderTableList() {
        let { list } = this.state;
        return (
            <tbody>
                {list.map((item, index) => {
                    return (
                        <tr key={index} id={"id-" + item.id}>
                            {this.renderRedirectToLink(item.name)}
                            {this.renderRedirectToLink(item.category)}
                            {this.renderRedirectToLink(item.lang)}
                            {this.renderRedirectToLink(item.created_at)}
                            <td>{item.op}</td>
                        </tr>
                    );
                })}
            </tbody>
        );
    }
    renderTableHead() {
        return (
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Category</th>
                    <th>Case</th>
                    <th>Creation Date</th>
                    <th>Actions</th>
                </tr>
            </thead>
        );
    }
}

