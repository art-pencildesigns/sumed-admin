import React from 'react';
import history from '../history';

function pageLink(props, name, url, icon) {
    // console.log({ url, name })
    return (
        <li className="nav-item pb-2">
            <a
                href="#"
                className={'nav-link p-3 ' + (props.location == url ? 'active' : '')}
                onClick={evt => {
                    evt.preventDefault();
                    window.$('.nav-link').removeClass('active');
                    window.$(evt.target).addClass('active');

                    history.push({
                        pathname: '/' + url,
                        search: ''
                    });
                }}
            >
                <i className={'fa mr-2 fa-' + icon}></i> {name}
            </a>
        </li>
    );
}

function SideNav(props) {
    return (
        <div className="col-lg-2 col-md-3 sideBar col-12" id="leftSideBar">
            <ul className="nav nav-pills flex-column mt-3 pt-4" role="tablist">
                {pageLink(props, 'Dashboard', 'dashboard', 'home')}
                {pageLink(props, 'Suppliers', 'suppliers-list', 'user-tie')}
                {pageLink(props, 'Activities', 'activities', 'tools')}
                {pageLink(props, 'Lists', 'lists', 'clipboard-list')}
                {pageLink(props, 'Committees', 'committees', 'layer-group')}
                {pageLink(props, 'Users', 'users', 'users-cog')}
            </ul>
            <div className="mt-5 sideFoot width100">
                <br />
                <p className="mt-5" id="firstSideFootLink">
                    <a href="mailto:sumed@sumed@org" className="textHelp">
                        <i className="fa fa-globe"></i> Help & Support
                    </a>
                </p>
                <p className="mb-4">
                    <a href="mailto:sumed@sumed@org" className="textHelp">
                        <i className="fa fa-location-arrow"></i> Report an issue
                    </a>
                </p>
            </div>
        </div>
    );
}

export default SideNav;
