import React from "react";
import history from "../history";

function Header(props) {
  return (
    <div className={props.className || "col-lg-4"}>
      <h3 className="capitilize">
        <i className={"fa fa-" + props.icon}></i> <b>{props.title}</b>
      </h3>
      {props.back ? (
        <a className='hand' onClick={evt => {
          if (props.to) history.push({ pathname: props.to, });
          else history.goBack();
        }}>
          <small className="fa fa-arrow-circle-left text-muted capitilize">
            &nbsp; {props.back}
          </small>
        </a>
      ) : (
          <small className="text-muted">{props.subTitle}</small>
        )
      }
    </div >
  );
}

export default Header;
