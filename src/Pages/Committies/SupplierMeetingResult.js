import React, { Component } from "react";
import Header from "../Header";
import { Query } from "../../Helpers/Constants";
import { TableView, GenerateTable3, confirmDelete } from "../TableView";
import { Modal, HeaderWithSearch, Input } from "../../Components";
import history from "../../history";
import { HandleErrors } from "../../Helpers/General";

export default class SupplierMeetingResult extends Component {
  state = {
    committee: {},
    meeting: {},
    emailTemplates: [],
    actionType: "",
    emailMsg: "",
    supplier: {},
    reasons: {},
    files: [],
    uploadedFileName: "",
    uploadingFile: false
  };

  activitiesIDsList = [];

  async refreshList() {
    let committeeID = this.props.match.params.id;
    let meetingID = this.props.match.params.meetingID;
    let supplierID = this.props.match.params.supplierID;
    const promise1 = await Query(`meeting/${meetingID}`);
    const meeting = promise1.data.data;
    const committee = { id: committeeID }

    const supplier = meeting.suppliers.find(sub => sub.id == supplierID);

    console.log({ meeting, supplier });
    // const promise2 = await Query(`committee/${id}`);
    // const committee = promise2.data.data;

    // let inThereMap = meeting.suppliers.map(item => item.id);
    // let selectFromArray = GenerateTable3(committee.suppliers, inThereMap);

    // console.log({ meeting, committee, selectFromArray });
    this.setState({ meeting, committee, supplier });
  }

  componentDidMount() {
    this.refreshList();
    Query("email-templates", "get").then(res => {
      let emailTemplates = res.data.data;

      emailTemplates = emailTemplates.filter(temp => {
        if (temp.category == "Supplier Activity Rejection") return true;
        if (temp.category == "Supplier Activity Approval") return true;
        return false;
      });
      this.setState({ emailTemplates })
    }).catch(HandleErrors)
    this.activitiesIDsList = [];
  }

  takeAction = ({ activity, action }) => evt => {
    evt.preventDefault();
    let { activities } = this.state;
    if (activity.status != "pending approval") {
      window.alert("You Already made your decision on this activity. If you didn't save yet please discard the changes.");
      return;
    }
    activity.status = action;
    this.setState({ activities });

  }


  change = params => evt => {
    evt.preventDefault();
    let value = evt.target.value;
    let { emailTemplates, emailMsg, supplier } = this.state;
    switch (params) {
      case "emailTemplate":
        if (!value) return;
        let email = emailTemplates.find((item) => item.id == value);
        emailMsg = "";
        emailMsg = supplier.lang == "en" ? email.content : email.content_ar;
        break;
      case "emailMsg":
        emailMsg = value;
        break;
      default:
        console.error("something went wrong");
    }
    this.setState({
      emailMsg
    });
  };

  deleteRow = ({ id }) => evt => {
    evt.preventDefault();
    let promise = Query(`supplier/${id}`, "delete");
    promise.then(res => {
      history.goBack();
    });
  }

  render() {
    let { meeting, committee, supplier, emailTemplates, emailMsg, reasons, files,
      uploadedFileName, uploadingFile
    } = this.state;
    if (!supplier.id) return <div></div>;
    return (
      <div className="p-0 m-3 mt-5 mb-5">
        <div className="row">
          <Header
            icon="thumbs-up"
            title={`Meeting Results: ${supplier.username}`}
            back={"Back To Meeting"}
            className="col-6"
          />

          <div className="col-6">
            <h5 className="float-right text-danger pt-3 text-capitalize hand" onClick={evt => {
              window.$("#confirmDeleteModal").modal("show");
              window.deleteParams = { parent: this, evt, listItem: { id: supplier.id } };
            }}>Delete {supplier.username}</h5>
          </div>
        </div>

        <div className="accordion mt-4 shadowBox radius bg-white p-3" id="accordionParent">
          <div className="p-3">
            <h5 className="mb-2 hedAccordian border-0">
              <div className="row">
                <div className="col-4"><b className="text-sm text-primary">Activities</b></div>
                <div className="col-4"><b className="text-sm text-primary">Actions</b></div>
                <div className="col-4"><b className="text-sm text-primary">Type a Reason</b></div>
              </div>
            </h5>
            <hr />
            {supplier.activities.map((activity, index2) => {
              // if (this.activitiesIDsList.includes(activity.id)) {
              //   return;
              // }
              // else {
              //   this.activitiesIDsList.push(activity.id);
              // }
              if (!reasons[activity.id]) reasons[activity.id] = activity.reason;
              const currentStatus = activity.status;
              const approvedClass = currentStatus == "approved" ? "text-success" : "text-muted";
              const rejectedClass = currentStatus == "rejected" ? "text-danger" : "text-muted";
              return <h5 className="mt-2 hedAccordian border-0" key={index2}>
                <div className="row">
                  <div className="col-4"><b className="text-sm text-primary">
                    {activity.ActivityTree.map((ac, i) => {
                      return <span key={i}>{ac} {i != activity.ActivityTree.length - 1 && <span className="text-warning"> > </span>}</span>
                    })}
                    {/* {activity.name} {activity.code == "other" && " -> " + activity.pivot.other} */}
                  </b></div>
                  <div className="col-4">
                    <span className={"hand text-sm mr-4 " + approvedClass} onClick={this.takeAction({ action: "approved", activity })}> <i className="fa fa-check"></i> Accepted</span>
                    <span className={"hand text-sm " + rejectedClass} onClick={this.takeAction({ action: "rejected", activity })}><i className="fa fa-times"></i> Rejected</span>
                  </div>
                  <div className="col-4">
                    <Input
                      placeholder="Reason"
                      type="text"
                      value={reasons[activity.id]}
                      onChange={evt => {
                        reasons[activity.id] = evt.target.value;
                        this.setState({ reasons })
                      }}
                      className="form-control customModel"
                      required
                    />
                  </div>
                </div>
              </h5>
            })}
          </div>
        </div >

        <div className="mt-5 shadowBox radius bg-white p-3">
          <h3 className="text-capitalize">Customize Feedback Email for {supplier.username}</h3>

          <div className="mt-4">
            <div className="col-12">
              <select name="" id="" className='form-control radius20 coolInput' onChange={this.change("emailTemplate")}>
                <option value="">Choose Email Template</option>
                {emailTemplates.map((email, index3) => {
                  return <option key={index3} value={email.id}>{email.name}</option>
                })}
              </select>
            </div>

            <div className="col-12 mt-4">
              <span className="text-capitalize text-muted">Report to {supplier.username}</span>
              <textarea className="form-control border0 mt-2 text-lg" rows="5" value={emailMsg} onChange={this.change("emailMsg")} placeholder="Feedback Email Message sent to supplier"></textarea>
            </div>
            <div className="col-12 mt-0 pt-0 mb-3">
              <hr />
              <span className="text-primary text-capitalize h4">ِAttachments</span>
            </div>


            {files.map((file, index) => {
              return <div className="row col-12 px-5" key={index}>
                <div className="col-11 mb-0 pb-0">
                  <p>{file.file.name}</p>
                </div>
                <div className="col-1 mb-0 pb-0">
                  <i className="fa fa-trash-alt text-danger float-right hand" onClick={this.deleteFile(file)} />
                </div>
              </div>
            })}

            <div className="col-12 mt-0 pt-0"><hr /></div>


            <div className="col-12 p-0 m-0 mt-3 mb-3">
              <input type="file" accept=".pdf,.doc,.docs,.jpg,.png" className="d-none" id="hiddenInput" onChange={this.uploadFile()} />
              <div className="uploadBox border-dashed radius" onClick={this.openUploadPanel()}>
                <i className="fa fa-cloud-upload-alt"></i>
              </div>
              {uploadingFile && <div className="progress">
                <div className="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" id="progressBar">
                  <span>{uploadedFileName}</span>
                </div>
              </div>}
            </div>
          </div>
        </div>

        <div className="col-12 mt-3 mb-5">
          <button onClick={this.saveChanges()} className="float-right btn btn-outline-suceess btnCreate mt-0 align-middle ml-5">Submit</button>
          <button onClick={_ => window.location.reload()} className="float-right btn text-muted mt-0 align-middle"><b>Discard</b></button>
        </div>
        <br />
      </div >
    );
  }


  deleteFile = file => evt => {
    evt.preventDefault();
    let { files } = this.state;
    files = files.filter(doc => doc.id != file.id);
    this.setState({ files });
  }

  openUploadPanel = params => evt => {
    evt.preventDefault();
    window.$("#hiddenInput").click();
  }
  uploadFile = params => evt => {
    evt.preventDefault();
    let files = evt.target.files;
    // for (var i = 0, file; file = files[i]; i++) {
    if (!files.length) return;
    let file = files[0];
    let ext = file.name.substr(file.name.lastIndexOf('.') + 1);
    if (!["pdf", "jpg", "png", "doc", "docx"].includes(ext.toLowerCase())) {
      window.alert("You can't upload files with these extension.");
      return;
    }
    let formData = new FormData();
    formData.append("names", file.name.replace(/ /g, "-"));
    formData.append("file", file);
    this.setState({ uploadedFileName: file.name, uploadingFile: true });
    Query("upload-file", "post", formData).then(res => {
      this.setState({ uploadingFile: false });
      let file_ids = res.data.data;
      let { files } = this.state;
      files.push({ id: file_ids[0], file })
      this.setState({ files });
      // Query("attach-meeting-file", "post", {
      //   meeting_id: this.state.meeting.id,
      //   documents: file_ids
      // }).then(res => {
      //   this.refreshList();
      // }).catch(this.errorHandling)
    }).catch(HandleErrors);
    // }
  }


  saveChanges = params => evt => {
    evt.preventDefault();
    let { emailMsg, supplier, reasons } = this.state;

    if (!emailMsg) {
      window.alert("Please write an email message to send to the user with the current updates.")
      return;
    }

    let activities = {};
    for (let activity of supplier.activities) {
      activities[activity.id] = activity.status;
      reasons[activity.id] = reasons[activity.id];
    }

    window.$(".loaderParent").show();
    Query("changeactivitystatus", "post", {
      supplier_id: supplier.id,
      activities,
      emailMsg,
      files: [],
      reasons
    }).then(results => {
      // let id = results.data.data.id; //setting the committee id
      // this.refreshList();
      // this.refreshSubList();
      // this.setState({ newObj: { ...newObj, id, step: 2 } });
      // this.refreshList();
      // window.$("#selectFromEmailTemplates").modal("hide");
      window.$(".loaderParent").hide();
      history.goBack();
    })
      .catch(HandleErrors);
  }




}
