import React from "react";
import { Link } from "react-router-dom";

export default props => {
    let committee = props.committee;
    return (
        <ul className="nav float-left customTab nav-pills float-right ">
            <li className="nav-item mr-1">
                <Link
                    className={
                        "nav-link text-center px-3 " +
                        (props.active == "meetings" ? "active" : "")
                    }
                    to={"/committees/" + committee.id + "/meetings"}
                >
                    <i className="fa fa-handshake mr-2"></i>
                    Meetings
        </Link>
            </li>
            <li className="nav-item mr-1">
                <Link
                    className={
                        "nav-link text-center px-3 " +
                        (props.active == "suppliers" ? "active" : "")
                    }
                    to={"/committees/" + committee.id + "/suppliers"}
                >
                    <i className="fa fa-user-tie mr-2"></i>
                    Suppliers
        </Link>
            </li>
            <li className="nav-item mr-1">
                <Link
                    className={
                        "nav-link text-center px-3 " +
                        (props.active == "members" ? "active" : "")
                    }
                    to={"/committees/" + committee.id + "/members"}
                >
                    <i className="fa fa-users-cog mr-2"></i>
                    Members
        </Link>
            </li>
        </ul>
    );
};
