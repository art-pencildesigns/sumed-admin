import React, { Component } from "react";
import Header from "../Header";
import { Query, APIDocumentsRoute } from "../../Helpers/Constants";
import { TableView, GenerateTable3 } from "../TableView";
import { Modal, HeaderWithSearch, Input } from "../../Components";
import { animateCSS, supplierStatusClass, printFile, supplierStatusNamingFix } from "../../Helpers/General";
import { MeetingBlock } from "./MeetingBlock";
import history from "../../history";

export default class ShowMeeting extends Component {
  state = {
    committee: {
      id: false
    },
    meeting: {
      id: false,
      description: "asdf",
      description_header: ""
    },
    selectFromArray: [],
    filteringText: "",
    uploadedFileName: "",
    uploadingFile: false
  };

  submitSelectingModal = params => evt => {
    evt.preventDefault();
    let { newObj, selectFromArray, committee, meeting } = this.state;
    let supplier_ids = selectFromArray
      .filter(item => item.checked)
      .map(item => {
        return item.id;
      });

    let promise = Query("crud-meeting-supplier", "post", {
      meeting_id: meeting.id,
      supplier_ids,
      delete: false
    }).then(res => {
      this.refreshList();
      this.setState({ newObj: { ...newObj, step: 2 } });
      window.$("#addSuppliers").modal("hide");
    });
  };

  async refreshList() {
    let id = this.props.match.params.id;
    let meetingID = this.props.match.params.meetingID;
    const promise1 = await Query(`meeting/${meetingID}`);
    const meeting = promise1.data.data;
    const promise2 = await Query(`committee/${id}`);
    const committee = promise2.data.data;

    let inThereMap = meeting.suppliers.map(item => item.id);
    let selectFromArray = GenerateTable3(committee.suppliers, inThereMap);

    console.log({ meeting, committee, selectFromArray });
    this.setState({ meeting, committee, selectFromArray });
  }

  async componentDidMount() {
    // animateCSS("#RightContent", "bounceOutRight", _ => {
    //   animateCSS("#RightContent", "bounceInRight");
    // });
    await this.refreshList();
  }

  change = params => evt => {
    evt.preventDefault();
    console.log("hello")
    let value = evt.target.value;
    let { filteringText, meeting } = this.state;
    let { description_header, description } = meeting;
    switch (params) {
      case "filteringText":
        filteringText = value;
        break;
      case "description_header":
        description_header = value;
        break;
      case "description":
        description = value;
        break;

      default:
        console.error("something went wrong");
    }
    // console.log({ meeting })
    this.setState({ filteringText, meeting: { ...meeting, description, description_header } });
  };

  updateSelectingModal = ({ selectItem }) => evt => {
    let { selectFromArray } = this.state;
    selectFromArray = selectFromArray.map(item => {
      if (item.id == selectItem.id) item.checked = !item.checked;
      return item;
    });
    this.setState({ selectFromArray });
  };

  renderAddSupplierModal() {
    let { selectFromArray, filteringText } = this.state;
    console.log({ selectFromArray });
    const regexp = new RegExp(filteringText, "ig");
    let renderedselectFromArray = selectFromArray.filter(
      item => regexp.test(item.username)
    );
    console.log("dododo", { renderedselectFromArray, selectFromArray });
    return (
      <Modal id="addSuppliers" lg>
        <form onSubmit={this.submitSelectingModal()}>
          <HeaderWithSearch
            title="Add Suppliers"
            value={filteringText}
            onChange={this.change("filteringText")}
          />
          <table className="table mt-2">
            <thead>
              <tr>
                <th>#</th>
                <th>Name</th>
                <th>Application Date</th>
                <th>Status</th>
                <th>Email</th>
                <th>Phone</th>
              </tr>
            </thead>
            <tbody>
              {renderedselectFromArray.map(selectItem => {
                return (
                  <tr
                    key={selectItem.id}
                    onClick={this.updateSelectingModal({ selectItem })}
                    className={
                      "hand " + (selectItem.checked ? "checkedTR" : "")
                    }
                  >
                    <td>
                      <input
                        type="checkbox"
                        checked={selectItem.checked}
                        onChange={_ => false}
                      />
                    </td>
                    <td>{selectItem.username}</td>
                    <td>{selectItem.date}</td>
                    <td className={"text-capitalize " + supplierStatusClass(selectItem.status)}>{supplierStatusNamingFix(selectItem.status)}</td>

                    <td>{selectItem.email}</td>
                    <td>{selectItem.phone}</td>
                  </tr>
                );
              })}
            </tbody>
          </table>
          <div className="modal-footer d-block px-5 border-top-0">
            <button
              type="button"
              className="bg-white text-muted border-0"
              data-dismiss="modal"
            >
              Add Later
            </button>
            <button
              type="button"
              className="btn btnContinue float-right"
              type="submit"
            >
              Save Changes
              <i className="fa fa-arrow-circle-right ml-4"></i>
            </button>
          </div>
        </form>
      </Modal>
    );
  }

  errorHandling(error) {
    console.error("request failed: ", {
      error,
      response: error.response,
      msg: error.response.data.message
    });
    window.alert(error.response.data.message);
  }


  meetingResults = meeting => evt => {
    evt.preventDefault();
    let uri = `committees/${this.state.committee.id}/meetings/${meeting.id}/results`
    window.location.hash = uri;
    return;
  }


  openUploadPanel = params => evt => {
    evt.preventDefault();
    window.$("#hiddenInput").click();
  }
  uploadFile = params => evt => {
    evt.preventDefault();
    let files = evt.target.files;
    for (var i = 0, file; file = files[i]; i++) {
      if(file.size <= 5000000) {
        let ext = file.name.substr(file.name.lastIndexOf('.') + 1);
        if (!["pdf", "jpg", "png", "doc", "docx"].includes(ext.toLowerCase())) {
          window.alert("You can't upload files with these extension.");
          return;
        }
        let formData = new FormData();
        formData.append("names", file.name.replace(/ /g, "-"));
        formData.append("file", file);
        this.setState({ uploadedFileName: file.name, uploadingFile: true });
        Query("upload-file", "post", formData).then(res => {
          this.setState({ uploadingFile: false });
          let file_ids = res.data.data[0].id;
          Query("attach-meeting-file", "post", {
            meeting_id: this.state.meeting.id,
            documents: [file_ids]
          }).then(res => {
            this.refreshList();
          }).catch(this.errorHandling)
        }).catch(this.errorHandling);
      } else {
        alert('can\'t upload files more than 5MB');
      }
      
    }
  }

  deleteFile = ({ fileId }) => evt => {
    evt.preventDefault();
    Query("attach-meeting-file", "post", {
      meeting_id: this.state.meeting.id,
      documents: [fileId]
    }).then(res => {
      this.refreshList();
    }).catch(this.errorHandling)
  }



  render() {
    let { committee, meeting, uploadedFileName, uploadingFile } = this.state;
    if (!meeting.id || !committee.id) return <div></div>;
    return (
      <div className="p-0 m-5 mt-5">
        {this.renderAddSupplierModal()}
        <div className="row">
          <Header
            icon="hand-shake"
            title={`Meeting: ${meeting.topic}`}
            back={"Back To Meetings"}
            to={`/committees/${committee.id}/meetings`}
            className="col-lg-4"
          />
          <MeetingBlock committeeId={committee.id} meetingId={meeting.id} />


        </div>


        <div className="mt-4 tab-content">
          <div className="tab-pane show fade p-0 active" id="details">
            <div className="row mt-4">
              <div className="col-lg-3">
                <h4 className="mb-3">Details</h4>
                <div className="details font-weight-bold bg-white p-4 shadowBox ">
                  <span className="text-muted">Date</span>
                  <p>{meeting.date}</p>
                  <span className="text-muted">Time</span>
                  <p>
                    {meeting.start_time} - {meeting.end_time}
                  </p>
                  <span className="text-muted">Suppliers</span>
                  <p className="mb-5">{meeting.suppliers.length || 0}</p>
                </div>
              </div>
              <div className="col-lg-9">
                <h4 className="mb-3">Members</h4>
                <TableView renderTableList={this.renderMembers.bind(this, "list")} renderTableHead={this.renderMembers.bind(this, "head")} />
              </div>
            </div>
            <div className="row mt-4">
              <div className="col-4">
                <h4 className="mb-3">Suppliers</h4>
              </div>
              <div className="col-8">
                <div className="float-right">
                  {/* <img src="images/arr.png" />
              <img src="images/hed.png" /> */}
                  <span className="ml-5">
                    <button
                      className="btn btn-outline-suceess btnCreate mt-0 align-middle"
                      onClick={evt => {
                        evt.preventDefault();
                        window.$("#addSuppliers").modal("show");
                      }}
                    >
                      <i className="fa fa-plus-square mr-1"></i> Add Suppliers
                </button>
                  </span>
                </div>
              </div>
              <div className="col-12">
                <TableView renderTableList={this.renderSuppliers.bind(this, "list")} renderTableHead={this.renderSuppliers.bind(this, "head")} />
              </div>
            </div>
            <div className="mt-4 mb-4">
              <h4 className="mb-3">Meeting Minutes</h4>
              <div className="bg-white px-5 py-4 shadowBox radius">
                <input className="form-control border-0 input-bold mb-2" placeholder="Type Subject" value={meeting.description_header || ""} onChange={this.change("description_header")} />
                <textarea className="form-control border-0" placeholder="Meeting Minutes Content" value={meeting.description || ""} onChange={this.change("description")} rows="5" />
                <div className="bg-white d-flex flex-row-reverse mt-2">
                  <button className="btn btnSaveMeet" onClick={this.editMeeting}>
                    Save <i className="far fa-check-circle ml-5" />
                  </button>
                  <button className="btn bg-white text-muted mr-4" onClick={evt => this.refreshList()}>Discard</button>
                </div>
              </div>
            </div>
          </div>


          <div className="tab-pane fade p-0 " id="attachments">
            <input type="file" accept=".pdf,.doc,.docs,.jpg,.png" className="d-none" id="hiddenInput" onChange={this.uploadFile()} multiple maxSize={1200000}/>
            <div className="shadowBox radius uploadBox" onClick={this.openUploadPanel()}>
              <i className="fa fa-cloud-upload-alt"></i>
            </div>
            {uploadingFile && <div className="progress">
              <div className="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" id="progressBar">
                <span>{uploadedFileName}</span>
              </div>
            </div>}

            <div className="shadowBox bg-white p-3 mt-5 radius">
              <h3 className="mb-3">Uploaded Documents</h3>
              {meeting.documents.map((file, index) => {
                return <div className="row" key={index}>
                  <a className="col-10 text-dark" href={file.path} download={file.name} target="_blank">{file.name}</a>
                  <div className="col-2">
                    <a onClick={this.deleteFile({ fileId: file.id })}><i className="fa fa-trash-alt mx-2 hand text-danger" /></a>
                    <a className="text-muted ml-2 hand" onClick={evt => printFile(file.path)}><i className="fa fa-print" /></a>
                    <a className="text-dark ml-2" href={file.path} download={file.name} target="_blank"><i className="fa fa-download fDownload" /></a>
                  </div>
                  <hr />
                </div>
              })}
            </div>
          </div>


          <div className="tab-pane fade p-0 " id="meetingResults">
            <h4 className="mt-3 mb-4 bold">Suppliers</h4>
            {meeting.suppliers && meeting.suppliers.map((supplier, index) => {
              return (<div className="card bg-white shadowBox radius bg-white p-2 mb-4 hand" key={index} onClick={evt => {
                history.push({ pathname: `/committees/${committee.id}/meetings/${meeting.id}/supplier/${supplier.id}` })
              }}>
                <div className="card-header bg-white border0" id={"header-" + index}>
                  <h5 className="mb-2 hedAccordian border-0">
                    <div className="row">
                      <div className="col"><span className="text-sm text-primary">Name</span> <br /> <span className="text-dark">{supplier.username}</span></div>
                      <div className="col"><span className="text-sm text-primary">Status</span> <br /> <span className={"text-capitalize " + supplierStatusClass(supplier.status)}>{supplierStatusNamingFix(supplier.status)}</span> </div>
                      <div className="col"><span className="text-sm text-primary">Type</span> <br /> <span className="text-dark text-capitalize">{supplier.type}</span></div>
                      <div className="col"><span className="text-sm text-primary">Applied On</span> <br /> <span className="text-dark">{supplier.date}</span></div>
                      <div className="col mt-3"><span className="text-warning  float-right align-middle">Action</span></div>
                    </div>
                    {/* <i className="float-right fas fa-angle-down rotate-icon" /> */}
                  </h5>
                </div>
              </div>
              )
            })}
          </div>


        </div>
      </div>
    );
  }

  editMeeting = evt => {
    let { description, description_header, id } = this.state.meeting;
    let promise = Query(`meeting/${id}`, "put", { description, description_header });
    promise
      .then(results => {
        this.refreshList();
        window.alert("Saved...");
      })
      .catch(this.errorHandling);
  }

  renderMembers(type = "list") {
    console.log({ type });
    let { committee } = this.state;
    let list = committee.members;
    if (type == "head")
      return (
        <thead>
          <tr>
            <th>Name</th>
            <th>Role</th>
            <th>Phone</th>
            <th>Email</th>
            {/* <th>Actions</th> */}
          </tr>
        </thead>
      );
    else if (type == "list")
      return (
        <tbody>
          {list.map((item, index) => {
            let statusClass = "capitilize ";
            if (item.status == "pending") statusClass += "text-muted";
            else if (item.status == "completed") statusClass += "text-success";
            else if (item.status == "inProgress") statusClass += "text-primary";
            return (
              <tr key={index} id={"id-" + item.id}>
                <td>{item.name}</td>
                <td>{item.roleName}</td>
                <td>{item.phone}</td>
                <td>{item.email}</td>
                {/* <td>{item.op}</td> */}
              </tr>
            );
          })}
        </tbody>
      );
  }

  renderSuppliers(type = "list") {
    console.log({ type });
    let { selectFromArray, meeting } = this.state;
    let list = meeting.suppliers;
    if (type == "head")
      return (
        <thead>
          <tr>
            <th>Supplier</th>
            <th>Status</th>
            <th>Type</th>
            <th>Application Date</th>
            {/* <th>Actions</th> */}
          </tr>
        </thead>
      );
    else if (type == "list")
      return (
        <tbody>
          {list.map((item, index) => {
            return (
              <tr key={index} id={"id-" + item.id} className="text-capitalize hand" onClick={evt => history.push({ pathname: "/suppliers/" + item.id })}>
                <td>{item.username}</td>
                <td className={"text-capitalize " + supplierStatusClass(item.status)}>{supplierStatusNamingFix(item.status)}</td>
                <td>{item.type}</td>
                <td>{item.date}</td>
                {/* <td>{item.op}</td> */}
              </tr>
            );
          })}
        </tbody>
      );
  }
}
