import React from "react";
import { Link } from "react-router-dom";

export default props => {
    return (
        <ul className="nav float-right customTab nav-pills ">
            <li className="nav-item">
                <Link
                    className={
                        "nav-link text-center px-3 " +
                        (props.active == "roles" ? "active" : "")
                    }
                    to={"/roles"}
                >
                    <i className="fa fa-user-shield mr-2"></i>
                    Roles
        </Link>
            </li>
            <li className="nav-item">
                <Link
                    className={
                        "nav-link text-center px-3 " +
                        (props.active == "emails" ? "active" : "")
                    }
                    to={"/emails"}
                >
                    <i className="fa fa-envelope-square mr-2"></i>
                    Email Templates
        </Link>
            </li>
        </ul>
    );
};
