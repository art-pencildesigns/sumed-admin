import React, { Component } from "react";
import {
  animateCSS,
  arrayUniq,
  supplierStatusClass,
  Naming,
  PrintContent,
  supplierStatusNamingFix
} from "../../Helpers/General";
import { Modal, Input, Button, HeaderWithSearch, Checkbox2 , ReminderModel} from "../../Components";
import { TableView, GenerateTable1, GenerateTable2 } from "../TableView";
import { Link } from "react-router-dom";
import Header from "../Header";
import { Query, PublicRoute, FrontendRoute } from "../../Helpers/Constants";
import Img from "../../Components/Img";
import { checkPermission, isSuperAdmin } from "../../Helpers/HelperComponenets";
import history from "../../history";
import debounce from "lodash.debounce";


export default class SuppliersList extends Component {
  state = {
    list: [],
    activities: { 1: [], 2: [], 3: [], 4: [], 5: [] },
    newObj: {
      id: false,
      name: "",
      company_name: "",
      email: "",
      password: "",
      password_confirmation: "",
      phone: "",
      business_type: [],
      for_company: [],
      account_type: [],
      national: "",
      step: 1,
    },
    selectFromArray: [],
    filteringText: "",
    FilterationSearch: "",
    FilterationBased: "",
    FilterationRegisteredFrom: "", FilterationRegisteredTo: "",
    FilterationApprovedFrom: "", FilterationApprovedTo: "",
    FilterationAccountType: [], FilterationBusinessType: [], FilterationStatus: [],
    FilterationActivity: "",
    current_page: 1,
    page_limit: 20,
    last_page: 1,
    FilterationActivityName: "",
    ShowSearchWhatBox: false
  };

  resetNewObj() {
    let newObj = {
      id: false,
      name: "",
      company_name: "",
      email: "",
      password: "",
      password_confirmation: "",
      phone: "",
      business_type: [],
      for_company: [],
      account_type: [],
      national: "",
      step: 1
    };
    this.setState({ newObj });
  }

  deleteRow = ({ id }) => evt => {
    evt.preventDefault();
    if (!checkPermission("suppliers")) return;
    let promise = Query(`committee/${id}`, "delete");
    promise.then(res => {
      let { list } = this.state;
      animateCSS(`#id-${id}`, "bounceOutLeft", _ => {
        list = list.filter(item => item.id != id);
        this.setState({ list });
      });
    });
  };

  showEditRow = ({ listItem }) => evt => {
    evt.preventDefault();
    let { newObj } = this.state;
    newObj.id = listItem.id;
    newObj.name = listItem.name;
    newObj.description = listItem.description;
    this.setState({ newObj });
    window.$("#editModal").modal("show");
  };

  editRow = () => evt => {
    evt.preventDefault();
    //TODO: update the backend
    let { name, description, id } = this.state.newObj;
    let promise = Query(`committee/${id}`, "put", { name, description });
    promise
      .then(results => {
        this.refreshList();
        this.resetNewObj();
        window.$("#editModal").modal("hide");
      })
      .catch(error => {
        console.error("request failed: ", {
          error,
          response: error.response,
          msg: error.response.data.message
        });
        window.alert(error.response.data.message);
      });
  };

  refreshList(options = null) {
    let { FilterationSearch, FilterationBased, FilterationRegisteredFrom, FilterationRegisteredTo, FilterationApprovedFrom, FilterationApprovedTo,
      FilterationAccountType, FilterationBusinessType, FilterationStatus, FilterationActivity,
      current_page, page_limit
    } = this.state;
    let searchObj = {
      search_value: FilterationSearch,
      activity_id: FilterationActivity,
      company_type: FilterationAccountType,
      national: [FilterationBased],
      business_type: FilterationBusinessType,
      joined_from: FilterationRegisteredFrom,
      joined_to: FilterationRegisteredTo,
      from: FilterationApprovedFrom,
      to: FilterationApprovedTo,
      status: FilterationStatus,
      page: current_page,
      pageLimit: page_limit
    }
    if (options == "printMode") {
      searchObj.page = 1;
      searchObj.pageLimit = 99999;
    }
    let promise = Query("supplier-search", "post", searchObj).then(res => {
      let pagi = res.data.data;
      let list = pagi.data;
      list = Object.values(list);
      list = GenerateTable1(list, this, { view: true, delete: false });
      this.setState({ list, last_page: pagi.last_page });
      if (options == "printMode") this.printResults()
    }).catch(this.errorHandling);
  }

  refreshSubList() {
    let promise2 = Query("admin-members").then(res => {
      let selectFromArray = res.data.data;
      selectFromArray = GenerateTable2(selectFromArray);
      this.setState({ selectFromArray });
    });
  }

  addRow = () => evt => {
    evt.preventDefault();
    let {
      name,
      company_name,
      email,
      password,
      password_confirmation,
      phone,
      business_type,
      for_company,
      account_type,
      national
    } = this.state.newObj;
    let { newObj } = this.state;
    let promise = Query("inviteSupplier", "post", {
      username: name,
      company_name,
      email,
      password,
      password_confirmation,
      phone,
      business_type,
      for_company: for_company.length == 2 ? "both" : for_company,
      type: account_type,
      national
    });
    promise
      .then(results => {
        let id = results.data.data.id; //setting the committee id
        this.refreshList();
        this.resetNewObj()
        setTimeout(() => {
          this.setState({ newObj: { ...newObj, step: 2 } });
        }, 500);
      })
      .catch(this.errorHandling);
  };

  errorHandling(error) {
    console.error("request failed: ", {
      error,
      response: error.response,
    });
    let errorMsg = error.response.data.message;
    if (error.response.data.errors ){
      if(Array.isArray(error.response.data.errors)){

        errorMsg += ". " + error.response.data.errors[0];
      }
      else{
        let key = Object.keys(error.response.data.errors)[0] 
        if(key == 'password'){
          errorMsg += ". " + error.response.data.errors.password;
        }
        else if(key == 'national' || key == 'business_type'){
          errorMsg += ". " +  key +" required";

        }
        else{
          errorMsg += ". " + key +" has already been taken";

        }

      }
    } 
    window.alert(errorMsg);
  }

  submitSelectingModal = params => evt => {
    evt.preventDefault();
    let { newObj, selectFromArray } = this.state;
    let admin_ids = selectFromArray
      .filter(item => item.checked)
      .map(item => {
        return item.id;
      });
    let promise = Query("crud-committee-member", "post", {
      committee_id: newObj.id,
      admin_ids,
      delete: false
    }).then(res => {
      this.refreshList();
      this.setState({ newObj: { ...newObj, step: 3 } });
    });
    // window.$("#createModal").modal("hide");
  };

  updateSelectingModal = ({ selectItem }) => evt => {
    let { selectFromArray } = this.state;
    selectFromArray = selectFromArray.map(item => {
      if (item.id == selectItem.id) item.checked = !item.checked;
      return item;
    });
    this.setState({ selectFromArray });
  };

  async componentDidMount() {
    if(this.props.match.params.status) this.state.FilterationStatus = [this.props.match.params.status];
    this.refreshList();
    let promise = Query("activities").then(res => {
      let activitiesList = res.data.data;
      let { activities } = this.state;
      this.setState({ activities: { ...activities, 1: activitiesList } });
    });
    // window.$("#advancedSearchModal").modal("show");
  }



  openUploadPanel = params => evt => {
    evt.preventDefault();
    if (!isSuperAdmin()) return;
    window.$("#hiddenInput").click();
  }
  uploadFile = params => evt => {
    evt.preventDefault();
    let files = evt.target.files;
    // for (var i = 0, file; file = files[i]; i++) {
    let formData = new FormData();
    formData.append("file", files[0]);
    Query("upload-bulk", "post", formData).then(res => {
      window.alert("Success");
      this.refreshList();
    }).catch(res => {
      let errors = res.response.data.data.errors;
      if (!errors.length) {
        this.errorHandling(res);
        return;
      }
      window.alert(errors[0]);
      window.location.reload();
    });
  }



  debounced = debounce(_ => {
    this.refreshList();
  }, 500);


  render() {
    let { list, FilterationSearch } = this.state;
    let listLength = list.length || 0;

    return (
      <div className="p-0 m-3 mt-5">
        {this.renderCreationModal()}
        {this.renderFilterationModal()}
        <div className="row">
          <Header
            icon="user-tie"
            title="Suppliers"
            subTitle={listLength + " Suppliers"}
          />

          <div className="col-lg-8">
            <div className="float-right">
              <span className="ml-5">

                <input type="file" accept=".xls,.xlsx" className="d-none" id="hiddenInput" onChange={this.uploadFile()} />
                {/* <input type="file" className="d-none" id="hiddenInput" onChange={this.uploadFile()} /> */}
                <div className="btn-group mr-1">
                  <button
                    className="btn btn-outline-suceess btnCreate"
                    onClick={this.openUploadPanel()}
                  > <i className="fa fa-cloud-upload-alt mr-1"></i> Add Bulk
                </button>
                  <button type="button" className="btn btn-outline-sucees btnCreate p-1 dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <span className="sr-only">Toggle Dropdown</span>
                  </button>
                  <div className="dropdown-menu">
                    <a className="dropdown-item" href={FrontendRoute + "Supplier-Template.xlsx"} download>Download .XLSX Template</a>
                  </div>
                </div>

                <button
                  className="btn btn-outline-suceess btnCreate mt-0 align-middle"
                  onClick={evt => {
                    if (!checkPermission("suppliers")) return;
                    evt.preventDefault();
                    this.resetNewObj();
                    window.$("#createModal").modal("show");
                  }}
                >
                  <i className="fa fa-plus-square mr-1"></i> Invite A Supplier
                </button>
                {/* <button onClick={evt => this.sendReminderAll()}  type="button" className="btn btnContinue float-right"> Remind All Supplier </button> */}

              </span>
            </div>
          </div>
        </div>


        <div className="col-12">
          <div className="input-group input-group-lg mt-3">
            <div className="input-group-prepend">
              <span className="input-group-text bg-white border0"> <i className="fa fa-search"></i></span>
            </div>
            <input type="text" className="form-control border0" placeholder="Search For A Supplier" aria-label="Search For A Supplier" aria-describedby="button-addon2"
              onChange={evt => {
                this.change("FilterationSearch", evt.target.value)(evt);
                this.debounced();
              }} value={FilterationSearch}
            />
            <div className="input-group-append">
              <button className="btn  bg-white border0" type="button" id="button-addon2" onClick={evt => {
                window.$("#advancedSearchModal").modal("show");
              }}><span> Advanced Search <span>&#9660;</span></span></button>
            </div>
          </div>
        </div>


        {this.renderSearchWhatBox()}

        {(list.length <= 0) ?
          <div className="p-5 text-center bg-white mt-3 shadowBox radius EmptyListMsg"><h5 className="text-muted">There are no suppliers with these filters</h5></div>
          : <div>
            <TableView
              renderTableList={this.renderTableList.bind(this)}
              renderTableHead={this.renderTableHead.bind(this)}
            />
            <nav aria-label="Page navigation" className="float-right mt-3">
              <ul className="pagination SumedPagination">
                {/* <li className="page-item"><a className="page-link text-muted" href="#">Prev.</a></li> */}
                {Array.from(Array(this.state.last_page)).map((item, index) => {
                  let className = "page-link ";
                  className += (this.state.current_page == index + 1) ? "active" : "text-muted";
                  return <li className="page-item" key={index}><a className={className} href="#" onClick={evt => {
                    evt.preventDefault();
                    this.setState({ current_page: (index + 1) }, _ => {
                      this.refreshList()
                    });
                  }}>{index + 1}</a></li>
                })}
                {/* <li className="page-item"><a className="page-link text-primary" href="#">Next</a></li> */}
              </ul>
            </nav>

            <button className="btn btn-outline-primary mt-3" onClick={evt => {
              evt.preventDefault();
              this.refreshList("printMode");
            }}>
              <i className="fa fa-print"></i> &nbsp;
              Print Results
              </button>

          </div>
        }
      </div>
    );
  }

  printResults = params => {
    let filterBox = window.$("#SearchWhatBox").html() || "";
    let table = window.$("#Table").html() || "";
    let printOut = `
    <style>
    .actions{
      display:none;
      opacity:0;
    }
    </style>
    <div class="row p-3">
      <h1 class="mb-4 text-justify text-center">Supplier List Results</h1>
      <br /> <br /> <br />
      <div style="border:1px dashed #ccc;width:100%" class="p-2 my-3">${filterBox} </div>
      <br /> <br />
      ${table}
    </div>`;
    PrintContent(printOut)
  }

  change = (params, passedValue) => evt => {
    evt.preventDefault();
    let value = passedValue || evt.target.value;
    let { newObj, filteringText, FilterationSearch, FilterationBased, FilterationRegisteredFrom, FilterationRegisteredTo, FilterationApprovedFrom, FilterationApprovedTo,
      FilterationAccountType, FilterationBusinessType, FilterationStatus,
      activities, FilterationActivity, FilterationActivityName, ShowSearchWhatBox
    } = this.state;
    let {
      name,
      company_name,
      email,
      password,
      password_confirmation,
      phone,
      business_type,
      for_company,
      account_type,
      national
    } = this.state.newObj;
    ShowSearchWhatBox = false;
    switch (params) {
      case "name": name = value; break;
      case "email": email = value; break;
      case "password": password = value; break;
      case "password_confirmation": password_confirmation = value; break;
      case "company_name": company_name = value; break;
      case "FilterationSearch": FilterationSearch = value; break;
      case "FilterationBased": FilterationBased = (FilterationBased != value) ? value : ""; break;
      case "FilterationRegisteredFrom": FilterationRegisteredFrom = value; break;
      case "FilterationRegisteredTo": FilterationRegisteredTo = value; break;
      case "FilterationApprovedFrom": FilterationApprovedFrom = value; break;
      case "FilterationApprovedTo": FilterationApprovedTo = value; break;
      case "FilterationActivity":
        let activityLevel = passedValue;
        let activityValue = evt.target.value;
        let currentLevel = passedValue;
        evt.persist();

        //if this is the empty (choose an activity) option
        if (!activityValue) {
          //empty all sub activities levels
          for (let i = 2; i < 6; i++) { activities[i] = []; }
          this.setState({ activities, FilterationActivity: "" });
          break;
        }

        Query(`activity/${activityValue}`).then(res => {
          let children = res.data.children;

          //empty all sub activities levels with higher level than current one. 
          for (let i = activityLevel + 1; i < 6; i++) { activities[i] = []; }

          //if it has no children & it'sn't the other activity
          if (children.length != 0 && res.data.code != "other") {
            activities[activityLevel + 1] = children;
            FilterationActivity = activityValue;
            this.setState({ activities, FilterationActivity });

            //if it has children or it's the other activity
          } else {
            FilterationActivity = activityValue;
            FilterationActivityName = evt.target.options[evt.target.selectedIndex].text;
            this.setState({ FilterationActivity, FilterationActivityName, ShowSearchWhatBox })
          }
          return;
        })
        break;
      case "FilterationAccountType":
        if (FilterationAccountType.includes(value)) {
          FilterationAccountType = FilterationAccountType.filter(item => item != value);
        } else FilterationAccountType = arrayUniq([...FilterationAccountType, value]);
        break;
      case "FilterationBusinessType":
        if (FilterationBusinessType.includes(value)) {
          FilterationBusinessType = FilterationBusinessType.filter(item => item != value);
        } else FilterationBusinessType = arrayUniq([...FilterationBusinessType, value]);
        break;
      case "FilterationStatus":
        if (FilterationStatus.includes(value)) {
          FilterationStatus = FilterationStatus.filter(item => item != value);
        } else FilterationStatus = arrayUniq([...FilterationStatus, value]);
        break;
      case "account_type":
        if (account_type.includes(value)) {
          account_type = account_type.filter(item => item != value);
        } else account_type = arrayUniq([...account_type, value]);
        if (account_type.length > 2) {
          window.alert("Can't choose three values");
          return;
        }
        break;
      case "business_type":
        if (value == "distributor" || business_type.includes("distributor")) business_type = [];
        if (business_type.includes(value)) {
          business_type = business_type.filter(item => item != value);
        } else business_type = arrayUniq([...business_type, value]);
        break;
      case "for_company":
        if (for_company.includes(value)) {
          for_company = for_company.filter(item => item != value);
        } else for_company = arrayUniq([...for_company, value]);
        break;
      case "national":
        national = value;
        break;
      case "phone":
        phone = value;
        break;
      case "filteringText":
        filteringText = value;
        break;
      default:
        console.error("something went wrong");
        window.alert("somemthing went wrong");
    }
    setTimeout(() => {
      this.setState(
        {
          newObj: {
            ...newObj,
            name,
            company_name,
            email,
            password,
            password_confirmation,
            phone,
            business_type,
            for_company,
            account_type,
            national
          },
          FilterationSearch, FilterationBased, FilterationRegisteredFrom, FilterationRegisteredTo, FilterationApprovedFrom, FilterationApprovedTo,
          FilterationAccountType, FilterationBusinessType, FilterationStatus,
          FilterationActivity, FilterationActivityName, ShowSearchWhatBox
        },
        _ => {
        }
      );
    }, 20);
  };

  renderSearchWhatBox() {
    let {
      ShowSearchWhatBox,
      FilterationSearch, FilterationBased, FilterationRegisteredFrom, FilterationRegisteredTo, FilterationApprovedFrom, FilterationApprovedTo,
      FilterationAccountType, FilterationBusinessType, FilterationStatus, FilterationActivityName
    } = this.state;
    if (
      !ShowSearchWhatBox || (
        FilterationSearch.length == 0
        && FilterationBased.length == 0
        && FilterationAccountType.length == 0
        && FilterationBusinessType.length == 0
        && FilterationStatus.length == 0
        && FilterationRegisteredFrom.length == 0
        && FilterationRegisteredTo.length == 0
        && FilterationApprovedFrom.length == 0
        && FilterationApprovedTo.length == 0
        && FilterationActivityName.length == 0
      )
    ) return <div></div>;
    return <div className="row mx-1 p-3 bg-white mt-3 shadowBox radius" id="SearchWhatBox">
      <div className="col-12 mb-1">
        <h5>Filtering By: </h5>
      </div>
      {FilterationSearch.length > 0 && <p className="col-6">Searching For: {FilterationSearch}</p>}
      {FilterationAccountType.length > 0 && <p className="col-6 text-capitalize">Supplier Type: {Naming(FilterationAccountType, ["supplier"], ["Service Provider/Contractor"]).join(", ")}</p>}
      {FilterationActivityName.length > 0 && <p className="col-6 text-capitalize">Activity: {FilterationActivityName}</p>}
      {FilterationBased.length > 0 && <p className="col-6">Supplier Based: {Naming(FilterationBased, ["1", "2"], ["Domestic", "International"])}</p>}
      {FilterationBusinessType.length > 0 && <p className="col-6 text-capitalize">Company Type: {FilterationBusinessType.join(", ")}</p>}
      {FilterationStatus.length > 0 && <p className="col-6 text-capitalize">Supplier Status: {Naming(FilterationStatus,
        ["not submited", "filtered"],
        ["Not Submitted", "Revised"]
      ).join(", ")}</p>}
      <div className="row col-12 p-0 m-0">
        {FilterationRegisteredFrom.length > 0 && <p className="col-6 text-capitalize">Registered From: {FilterationRegisteredFrom}</p>}
        {FilterationRegisteredTo.length > 0 && <p className="col-6 text-capitalize">Registered To: {FilterationRegisteredTo}</p>}
      </div>
      <div className="row col-12 p-0 m-0">
        {FilterationApprovedFrom.length > 0 && <p className="col-6 text-capitalize">Approved From: {FilterationApprovedFrom}</p>}
        {FilterationApprovedTo.length > 0 && <p className="col-6 text-capitalize">Approved From: {FilterationApprovedTo}</p>}
      </div>

    </div>
  }

  renderEditModal() {
    let { newObj } = this.state;
    return (
      <Modal id="editModal" onSubmit={this.editRow()}>
        <form onSubmit={this.editRow()}>
          <h2 className="pb-4">Edit Committee</h2>
          <Input
            label="Committee Name"
            type="text"
            value={newObj.name}
            onChange={this.change("name")}
            className="form-control customModel"
          />
          <Input
            label="Committee Description"
            type="text"
            value={newObj.description}
            onChange={this.change("description")}
            className="form-control customModel"
          />
          <div className="modal-footer d-block px-5 border-top-0">
            <button
              type="button"
              className="bg-white text-muted border-0"
              data-dismiss="modal"
            >
              Cancel
            </button>
            <button
              type="button"
              className="btn btnContinue float-right"
              type="submit"
            >
              Save
              <i className="fa fa-arrow-circle-right ml-4"></i>
            </button>
          </div>
        </form>
      </Modal>
    );
  }

  renderSelectingModal() {
    let { selectFromArray, filteringText } = this.state;
    const regexp = new RegExp(filteringText, "ig");
    let renderedselectFromArray = selectFromArray.filter(
      item => regexp.test(item.name) || regexp.test(item.email)
    );
    return (
      <form onSubmit={this.submitSelectingModal()}>
        <HeaderWithSearch
          title="Add Members"
          value={filteringText}
          onChange={this.change("filteringText")}
        />
        <table className="table mt-2">
          <thead>
            <tr>
              <th>#</th>
              <th>Name</th>
              <th>Role</th>
              <th>Email</th>
            </tr>
          </thead>
          <tbody>
            {renderedselectFromArray.map(selectItem => {
              return (
                <tr
                  key={selectItem.id}
                  onClick={this.updateSelectingModal({ selectItem })}
                  className={"hand " + (selectItem.checked ? "checkedTR" : "")}
                >
                  <td>
                    <input
                      type="checkbox"
                      checked={selectItem.checked}
                      onChange={_ => false}
                    />
                  </td>
                  <td>{selectItem.name}</td>
                  <td>{selectItem.role_name}</td>
                  <td>{selectItem.email}</td>
                </tr>
              );
            })}
          </tbody>
        </table>
        <div className="modal-footer d-block px-5 border-top-0">
          <button
            type="button"
            className="bg-white text-muted border-0"
            data-dismiss="modal"
          >
            Add Later
          </button>
          <button
            type="button"
            className="btn btnContinue float-right"
            type="submit"
          >
            Add
            <i className="fa fa-arrow-circle-right ml-4"></i>
          </button>
        </div>
      </form>
    );
  }

  renderFilterationModal() {
    let { newObj, activities,
      FilterationSearch, FilterationBased, FilterationRegisteredFrom, FilterationRegisteredTo, FilterationApprovedFrom, FilterationApprovedTo,
      FilterationAccountType, FilterationBusinessType, FilterationStatus
    } = this.state;
    return (
      <Modal id="advancedSearchModal" lg>
        <form onSubmit={evt => {
          evt.preventDefault();
          this.refreshList();
          this.setState({ ShowSearchWhatBox: true })
          window.$("#advancedSearchModal").modal("hide");
        }} className="p-3">
          {/* <h2 className="pb-4">Invite Supplier</h2> */}
          <div className="row">

            <Input
              label="Search"
              type="text"
              onChange={this.change("FilterationSearch")}
              className="form-control customModel"
              lableClassName="text-primary text-sm"
              parentClass="col-12"
            />

            <div className="col-12 mt-3 bg-gray radius">
              <div className="row mt-2 py-3">
                <div className="col-12 text-primary text-sm mb-2"><label><b>Add Supplier Activity</b> </label><br /></div>
                <Input
                  type="text"
                  onChange={this.change("FilterationActivity")}
                  className="form-control customModel"
                  parentClass="col-6"
                  hideInput={true}
                >
                  <select className="form-control customModel bg-gray" value={newObj.FilterationActivity} onChange={this.change("FilterationActivity", 1)}>
                    <option value="">Choose An Activity</option>
                    {activities[1].map((activity, index) => <option key={activity.id} value={activity.id}>{activity.name}</option>)}
                  </select>
                </Input>

                {activities[2].length > 0 && <Input
                  type="text"
                  onChange={this.change("FilterationActivity")}
                  className="form-control customModel"
                  parentClass="col-6"
                  hideInput={true}
                >
                  <select className="form-control customModel bg-gray" value={newObj.FilterationActivity} onChange={this.change("FilterationActivity", 2)}>
                    <option value="" disabled>Choose An Activity</option>
                    {activities[2].map((activity, index) => <option key={activity.id} value={activity.id}>{activity.name}</option>)}
                  </select>
                </Input>}

                {activities[3].length > 0 && <Input
                  type="text"
                  onChange={this.change("FilterationActivity")}
                  className="form-control customModel"
                  parentClass="col-6"
                  hideInput={true}
                >
                  <select className="form-control customModel bg-gray" value={newObj.FilterationActivity} onChange={this.change("FilterationActivity", 3)}>
                    <option value="" disabled>Choose An Activity</option>
                    {activities[3].map((activity, index) => <option key={activity.id} value={activity.id}>{activity.name}</option>)}
                  </select>
                </Input>}

                {activities[4].length > 0 && <Input
                  type="text"
                  onChange={this.change("FilterationActivity")}
                  className="form-control customModel"
                  parentClass="col-6"
                  hideInput={true}
                >
                  <select className="form-control customModel bg-gray" value={newObj.FilterationActivity} onChange={this.change("FilterationActivity", 4)}>
                    <option value="" disabled>Choose An Activity</option>
                    {activities[4].map((activity, index) => <option key={activity.id} value={activity.id}>{activity.name}</option>)}
                  </select>
                </Input>}

                {activities[5].length > 0 && <Input
                  type="text"
                  onChange={this.change("FilterationActivity")}
                  className="form-control customModel"
                  parentClass="col-6"
                  hideInput={true}
                >
                  <select className="form-control customModel bg-gray" value={newObj.FilterationActivity} onChange={this.change("FilterationActivity", 5)}>
                    <option value="" disabled>Choose An Activity</option>
                    {activities[5].map((activity, index) => <option key={activity.id} value={activity.id}>{activity.name}</option>)}
                  </select>
                </Input>}

              </div>
            </div>

            <div className="col-12 p-0 mt-3">
              <div className="col-12 text-primary text-sm mb-1"><label><b>Supplier Type</b> </label><br /></div>
              <div className="row px-3">
                <div className="col-5">
                  <Checkbox2 title="Service Provider/Contractor" value={FilterationAccountType.includes("supplier")} onClick={this.change("FilterationAccountType", "supplier")} />
                </div>
                <div className="col-3">
                  <Checkbox2 title="Bidder" value={FilterationAccountType.includes("bidder")} onClick={this.change("FilterationAccountType", "bidder")} />
                </div>
                <div className="col-4">
                  <Checkbox2 title="Vendor" value={FilterationAccountType.includes("vendor")} onClick={this.change("FilterationAccountType", "vendor")} />
                </div>
              </div>
            </div>


            <div className="col-12 p-0 mt-1">
              <div className="col-12 text-primary text-sm"><label><b>Supplier Based</b> </label><br /></div>
              <div className="row px-3">
                <div className="col-4">
                  <Checkbox2 title="Domestic" value={FilterationBased == 1} onClick={this.change("FilterationBased", "1")} />
                </div>
                <div className="col-4">
                  <Checkbox2 title="International" value={FilterationBased == 2} onClick={this.change("FilterationBased", "2")} />
                </div>
              </div>
            </div>



            <div className="col-12 p-0 mt-1">
              <div className="col-12 text-primary text-sm mb-1"><label><b>Company Type</b> </label><br /></div>
              <div className="row px-3">
                <div className="col-4">
                  <Checkbox2 title="Agent" value={FilterationBusinessType.includes("agent")} onClick={this.change("FilterationBusinessType", "agent")} />
                </div>
                <div className="col-4">
                  <Checkbox2 title="Company" value={FilterationBusinessType.includes("company")} onClick={this.change("FilterationBusinessType", "company")} />
                </div>
                <div className="col-4">
                  <Checkbox2 title="Distributor" value={FilterationBusinessType.includes("distributor")} onClick={this.change("FilterationBusinessType", "distributor")} />
                </div>
              </div>
            </div>


            <div className="col-12 p-0 mt-1">
              <div className="col-12 text-primary text-sm mb-1"><label><b>Supplier Status</b> </label><br /></div>
              <div className="row px-3">
                <div className="col-4">
                  <Checkbox2 title="Not Submitted" value={FilterationStatus.includes("not submited")} onClick={this.change("FilterationStatus", "not submited")} />
                </div>
                <div className="col-4">
                  <Checkbox2 title="Pending Approval" value={FilterationStatus.includes("pending approval")} onClick={this.change("FilterationStatus", "pending approval")} />
                </div>
                <div className="col-4">
                  <Checkbox2 title="Revised" value={FilterationStatus.includes("filtered")} onClick={this.change("FilterationStatus", "filtered")} />
                </div>
                <div className="col-4">
                  <Checkbox2 title="Approved" value={FilterationStatus.includes("approved")} onClick={this.change("FilterationStatus", "approved")} />
                </div>
                <div className="col-4">
                  <Checkbox2 title="Rejected" value={FilterationStatus.includes("rejected")} onClick={this.change("FilterationStatus", "rejected")} />
                </div>
                <div className="col-4">
                  <Checkbox2 title="Frozen" value={FilterationStatus.includes("frozen")} onClick={this.change("FilterationStatus", "frozen")} />
                </div>
              </div>
            </div>



            <div className="col-12 p-0 mt-3">
              <div className="row px-3">
                <Input
                  label="Registered From"
                  type="date"
                  value={newObj.FilterationRegisteredFrom}
                  onChange={this.change("FilterationRegisteredFrom")}
                  className="form-control dateBox"
                  parentClass="col-3"
                />
                <Input
                  label="Registered To"
                  type="date"
                  value={newObj.FilterationRegisteredTo}
                  onChange={this.change("FilterationRegisteredTo")}
                  className="form-control dateBox"
                  parentClass="col-3"
                />
                <Input
                  label="Approved From"
                  type="date"
                  value={newObj.FilterationApprovedFrom}
                  onChange={this.change("FilterationApprovedFrom")}
                  className="form-control dateBox"
                  parentClass="col-3"
                />
                <Input
                  label="Approved To"
                  type="date"
                  value={newObj.FilterationApprovedTo}
                  onChange={this.change("FilterationApprovedTo")}
                  className="form-control dateBox"
                  parentClass="col-3"
                />
              </div>
            </div>




          </div>
          <div className="modal-footer d-block px-5 border-top-0 mt-4">
            <button
              type="reset"
              className="bg-white text-muted border-0 px-4"
              data-dismiss="modal"
              onClick={evt => window.location.reload()}
            >
              Reset
          </button>
            <button
              type="button"
              className="btn btnContinue float-right px-4"
              type="submit"
            >
              Apply
            <i className="fa fa-arrow-circle-right ml-4"></i>
            </button>
          </div>
        </form>
      </Modal >
    );
  }
  renderReminderModal() {
    let { newObj, activities
    } = this.state;
    return (
      <Modal id="ReminderModal" lg>
        <form onSubmit={evt => {
          evt.preventDefault();
          this.refreshList();
          this.setState({ ShowSearchWhatBox: true })
          window.$("#ReminderModal").modal("hide");
        }} className="p-3">
          {/* <h2 className="pb-4">Invite Supplier</h2> */}
          <div className="row">
            <div class="form-group">
              <Input 
              label="Subject"
              type="text"
              className="form-control customModel"
              lableClassName="text-primary text-sm"
              parentClass="col-12"/>
            </div>
            <div class="form-group">
              <Input 
              label="Subject"
              type="text"
              className="form-control customModel"
              lableClassName="text-primary text-sm"
              parentClass="col-12"/>
            </div>

          </div>
          <div className="modal-footer d-block px-5 border-top-0 mt-4">
            <button
              type="reset"
              className="bg-white text-muted border-0 px-4"
              data-dismiss="modal"
              onClick={evt => window.location.reload()}
            >
              Close
          </button>
            <button
              type="button"
              className="btn btnContinue float-right px-4"
              type="submit"
            >
              Send
            <i className="fa fa-arrow-circle-right ml-4"></i>
            </button>
          </div>
        </form>
      </Modal >
    );
  }

  renderCreateObj() {
    let { newObj } = this.state;
    return (
      <form onSubmit={this.addRow()} className="p-3">
        <h2 className="pb-4">Invite Supplier</h2>
        <div className="row">

          <Input
            label="Supplier Name"
            type="text"
            value={newObj.name}
            onChange={this.change("name")}
            className="form-control customModel"
            parentClass="col-6"
            required
          />

          <Input
            label="Company Name"
            type="text"
            value={newObj.company_name}
            onChange={this.change("company_name")}
            className="form-control customModel"
            parentClass="col-6"
            required
          />

          <Input
            label="Password"
            type="password"
            value={newObj.password}
            onChange={this.change("password")}
            className="form-control customModel"
            parentClass="col-6 mt-4"
            required
          />

          <Input
            label="Confirm Password"
            type="password"
            value={newObj.password_confirmation}
            onChange={this.change("password_confirmation")}
            className="form-control customModel"
            parentClass="col-6 mt-4"
            required
          />

          <Input
            label="Email"
            type="text"
            value={newObj.email}
            onChange={this.change("email")}
            className="form-control customModel"
            parentClass="col-6 mt-4"
            required
          />

          <Input
            optional
            label="Phone"
            type="number"
            value={newObj.phone}
            onChange={this.change("phone")}
            className="form-control customModel"
            parentClass="col-6 mt-4"
          />


          <Input
            label="Nature of your company"
            type="text"
            value={newObj.account_type}
            onChange={this.change("account_type")}
            className="form-control customModel"
            parentClass="col-12 mt-4"
            hideInput={true}
          >
            <ul className="list-group list-group-horizontal border-0 mt-2">
              <li className={"list-group-item hand " + (newObj.account_type.includes("supplier") ? "active" : "")} onClick={this.change("account_type", "supplier")}>Service Provider / Contractor</li>
              <li className={"list-group-item hand " + (newObj.account_type.includes("vendor") ? "active" : "")} onClick={this.change("account_type", "vendor")}>Vendor</li>
              <li className={"list-group-item hand " + (newObj.account_type.includes("bidder") ? "active" : "")} onClick={this.change("account_type", "bidder")}>Bidder</li>
            </ul>
          </Input>



          <Input
            label="Company Based In ?"
            type="text"
            value={newObj.national}
            onChange={this.change("national")}
            className="form-control customModel"
            parentClass="col-12 mt-4"
            hideInput={true}
          >
            <select className="form-control customModel" value={newObj.national} onChange={this.change("national")}>
              <option value="" disabled>Choose Company Based In :-</option>
              <option value="0">Domestic</option>
              <option value="1">International</option>
            </select>
          </Input>




          <Input
            label="Provide Through ?"
            type="text"
            value={newObj.business_type}
            onChange={this.change("business_type")}
            className="form-control customModel"
            parentClass="col-12 mt-4"
            hideInput={true}
          >
            <ul className="list-group list-group-horizontal border-0 mt-2">
              <li className={"list-group-item hand " + (newObj.business_type.includes("company") ? "active" : "")} onClick={this.change("business_type", "company")}>Through our company</li>
              <li className={"list-group-item hand " + (newObj.business_type.includes("agent") ? "active" : "")} onClick={this.change("business_type", "agent")}>Through the Agent</li>
              <li className={"list-group-item hand " + (newObj.business_type.includes("distributor") ? "active" : "")} onClick={this.change("business_type", "distributor")}>Through Distributor</li>
            </ul>
          </Input>


          {newObj.business_type.includes("company") &&
            <Input
              label="Is supplier an Agent/Distributor for another company ?"
              type="text"
              value={newObj.for_company}
              onChange={this.change("for_company")}
              className="form-control customModel"
              parentClass="col-12 mt-4"
              hideInput={true}
            >
              <ul className="list-group list-group-horizontal border-0 mt-2">
                <li className={"list-group-item hand " + (newObj.for_company.includes("agent") ? "active" : "")} onClick={this.change("for_company", "agent")}>Agent</li>
                <li className={"list-group-item hand " + (newObj.for_company.includes("distributor") ? "active" : "")} onClick={this.change("for_company", "distributor")}>Distributor</li>
              </ul>
            </Input>
          }



        </div>
        <div className="modal-footer d-block px-5 border-top-0 mt-4">
          <button
            type="button"
            className="bg-white text-muted border-0"
            data-dismiss="modal"
          >
            Cancel
          </button>
          <button
            type="button"
            className="btn btnContinue float-right p-2"
            type="submit"
          >
            Add Supplier & Send Invitation
            <i className="fa fa-arrow-circle-right ml-4"></i>
          </button>
        </div>
      </form>
    );
  }

  renderSuccessMessage() {
    return (
      <div className="modal-body p-5">
        <img
          src="images/correct.svg"
          className="imgSuccess mr-auto ml-auto d-block pt-3"
        />
        <h4 className="font-weight-bold text-center pt-5 pb-3">
          Supplier Added & The Invitation sent.
        </h4>
      </div>
    );
  }

  renderCreationModal() {
    let { newObj } = this.state;
    return (
      <Modal id="createModal" lg>
        {newObj.step == 1 && this.renderCreateObj()}
        {/* {newObj.step == 2 && this.renderSelectingModal()} */}
        {newObj.step == 2 && this.renderSuccessMessage()}
      </Modal>
    );
  }


  gotoView(id) {
    history.push({
      pathname: `/suppliers/${id}`,
      search: ''
    })
  }
  sendReminder(id) {
    let promise = Query(`send-reminder/${id}`, "get", {}).then(res => {
    });
    
  }
  sendReminderAll() {
    let promise = Query(`send-all-reminder`, "get", {}).then(res => {
    });
    
  }
  renderLink(id, txt) {
    return (
      <td onClick={evt => this.gotoView(id)} className="hand text-capitalize"> {txt} </td>
    );
  }
  viewRow = ({ listItem }) => evt => {
    evt.preventDefault();
    this.gotoView(listItem.id);
  }
  sendReminderHandler(id,docs){
    this.props.sendreminderEmail(id,docs)
  }
  renderTableList =(sendreminderEmail) => {
    let { list } = this.state;
    return (<>
            <tbody>
            {list.map((item, index) => {
              // let activities = item.ActivityParentNames.join(", ");
              // try {
              let activities = Object.values(item.ActivityParentNames);
              return (
                <tr key={index} id={"id-" + item.id}>
                  {this.renderLink(item.id, <span> <Img src={item.image} style={{ "maxWidth": "4rem" }} /> {item.username} </span>)}
                  {this.renderLink(item.id, <span>  {item.company_name} </span>)}
                  {this.renderLink(item.id, <span className={supplierStatusClass(item.status)}>{supplierStatusNamingFix(item.status)}{(item.is_assigned && item.status == "filtered") && <span>/Assigned</span>}</span>)}
                  {this.renderLink(item.id, <span>{activities.map((ac, i) =>
                    <div key={i} className={supplierStatusClass(ac.status)}>
                      {ac.code == "other" ? ac.ChildName : ac.name}
                    </div>
                  )}</span>)}
                  {/* {this.renderLink(item.id, activities)} */}
                  {this.renderLink(item.id, item.creationDate)}
                  {this.renderLink(item.id, <span>{item.email} <br /> {item.phone_code}{item.phone}</span>)}
                  <td className="actions">
                  {
                    item.status == 'not submited' ? <button className="btn btnContinue float-right" type="button" data-toggle="modal" data-target="#ReminderModal" 
                    onClick={()=>{
                      sendreminderEmail(item.id, item.missing_docs)     
                                      }} 
                    data-whatever="@getbootstrap"> Reminder <span >{item.reminder}</span></button> : ''
                  } 
                    
                  </td>
                  <td className="actions">{item.op}</td>
                </tr>
              );
            })}
          </tbody>
          
          </>
      
    );
  }
  renderTableHead() {
    return (
      <thead>
        <tr>
          {/* <span>&#9650;</span> <span>&#9660;</span> */}
          <th>Name  </th>
          <th>Company Name  </th>
          <th>Status  </th>
          <th>Activity   </th>
          <th>Joining Date</th>
          <th>Contact</th>
          <th>Send Reminder</th>
          <th className="actions">Actions</th>
        </tr>
      </thead>
    );
  }
}

