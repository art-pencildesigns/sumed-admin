import React, { Component } from "react";
import TopNavBar from "./TopNavbar";
import SideNav from "./SideNav";
import { Query } from "../Helpers/Constants";
import { Modal } from "../Components";

export default class MainPage extends Component {
  state = {
    permissions: [],
    //   pathname: "none",
    //   requestsCount: 0
  };

  componentDidMount() {
    if (localStorage.getItem("access_token")) {
      Query("me").then(res => {
        const pathname = this.props.location.pathname;
        // console.log({ props: this.props });
        const userProfile = res.data.data;
        console.log({ userProfile })
        this.setState({
          pathname,
          permissions: userProfile.roleFunctions,
          requestsCount: userProfile.requestsCount
        });
      }).catch(error => { window.location.hash = ""; });
    } else {
      window.location.hash = "";
    }
  }

  render() {
    let { pathname } = this.state;
    let { permissions } = this.props.MainState;
    if (pathname == "none") return <div></div>;

    console.log({ state: this.props.MainState });
    let notPermitted = false;
    // console.log({ pathname, permissions }, permissions.committees.access);

    if (/suppliers/.test(pathname) && !permissions.suppliers.access) {
      notPermitted = true;
    }
    if (/activities/.test(pathname) && !permissions.activities.access) {
      notPermitted = true;
    }
    if (/lists/.test(pathname) && !permissions.lists.access) {
      notPermitted = true;
    }
    if (/committees/.test(pathname) && !permissions.committees.access) {
      notPermitted = true;
    }
    if (/users/.test(pathname) && !permissions.users.access) {
      notPermitted = true;
    }
    if (/requests/.test(pathname) && !permissions.requests.access) {
      notPermitted = true;
    }
    if (/(emails|roles)/.test(pathname) && !permissions.manage.access) {
      notPermitted = true;
    }

    return notPermitted ? this.renderPermissionErrorMessage() : this.props.Comp;
  }

  renderPermissionErrorMessage() {
    return (
      <div className="p-5 text-center bg-white mt-3 shadowBox radius">
        <h1>Sorry!</h1>
        <h4>Your role don't have premission to access this page. <br /> Please contact the admin for more information.</h4>
      </div>
    );
  }

}
