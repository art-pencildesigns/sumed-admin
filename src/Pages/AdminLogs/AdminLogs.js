import React, { Component } from 'react'
import { Query } from '../../Helpers/Constants'
import { HandleErrors } from '../../Helpers/General'
import Header from '../Header'

export default class AdminLogs extends Component {
    state = {
        adminlogs: []
    }
    componentDidMount() {
        Query("adminLogs").then(res => {
            this.setState({ adminlogs: res.data.data })
        }).catch(HandleErrors)
    }

    render() {
        let { adminlogs } = this.state;
        return (
            <div className="p-0 m-3 mt-5">
                <Header
                    icon="users-cog"
                    title="Admin Logs"
                    back="Back"
                />
                {(adminlogs <= 0) ?
                    <div className="p-5 text-center bg-white mt-3 shadowBox radius EmptyListMsg"><h5 className="text-muted">There are no Logs For this supplier</h5></div>
                    :
                    <div className="mt-4 p-4 logs shadowBox radius ">
                        {adminlogs.map((log, index) => {
                            return (<div className="row" key={index}>
                                {index >= 1 && <div className="col-12">
                                    <hr className="mt-1" />
                                </div>}
                                <div className="col-lg-8">
                                    <p>{log.content}</p>
                                </div>
                                <div className="col-lg-4">
                                    <p className="float-right"><b>{log.creationDate}</b></p>
                                </div>
                            </div>
                            )
                        })}
                    </div>
                }
            </div>
        )
    }
}
