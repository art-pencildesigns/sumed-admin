import React, { Component } from "react";
import Header from "../Header";
import { Query } from "../../Helpers/Constants";
import { animateCSS } from "../../Helpers/General";
import { Link } from "react-router-dom";

export default class Search extends Component {
  state = {
    searchWord: "",
    searchResponse: {
      suppliers: [],
      committees: [],
      lists: [],
      admin_users: [],
      admin_users: []
    }
  };

  async searchTheWord(searchWord) {
    let searchResponse = await Query("search", "post", { key: searchWord });
    searchResponse = searchResponse.data.data;
    console.log({ searchResponse });
    if (!searchResponse.committees) searchResponse.committees = [];
    if (!searchResponse.lists) searchResponse.lists = [];
    this.setState({ searchResponse });
  }

  async componentDidMount() {
    let searchWord = this.props.match.params.searchWord;
    await this.searchTheWord(searchWord);
    this.setState({ searchWord });
  }

  render() {
    // console.log(this.props.history);
    let { searchResponse, searchWord } = this.state;
    return (
      <div className="p-0 m-3 mt-5">
        <Header title={`Searching: ` + searchWord} icon="search" />

        {(searchResponse.committees <= 0 && searchResponse.suppliers <= 0 && searchResponse.lists <= 0 && searchResponse.admin_users <= 0) ?
          <div className="p-5 text-center bg-white mt-3 shadowBox radius EmptyListMsg"><h5 className="text-muted">There are no results</h5></div>
          :
          <div className="col-12">
            {this.renderSuppliers(searchResponse.suppliers)}
            {this.renderCommittees(searchResponse.committees)}
            {this.renderLists(searchResponse.lists)}
            {this.renderUsers(searchResponse.admin_users)}
          </div>
        }

      </div>
    );
  }

  renderUsers(admin_users) {
    return (
      <div className="row mt-5">
        <p className="col-12 bluish">System Users</p>
        <div
          className="col-12 table-responsive shadowBox radius p-0"
          id="Table"
        >
          <table className="table m-0">
            <thead>
              <tr>
                <th>Name</th>
                <th>Role</th>
                <th>Mobile No.</th>
                <th>Email</th>
              </tr>
            </thead>
            <tbody>
              {admin_users.map((user, index) => {
                return (
                  <tr>
                    <td>{user.name}</td>
                    <td>{user.roleName}</td>
                    <td>{user.phone}</td>
                    <td>{user.email}</td>
                  </tr>
                );
              })}
            </tbody>
          </table>
        </div>
      </div>
    );
  }

  renderLists(lists) {
    return (
      <div className="row mt-5">
        <p className="col-12 bluish">Lists</p>
        <div
          className="col-12 table-responsive shadowBox radius p-0"
          id="Table"
        >
          <table className="table m-0">
            <thead>
              <tr>
                <th>Name</th>
                <th>Creation Date</th>
                <th>Number of Supplier</th>
                <th>Description</th>
              </tr>
            </thead>
            <tbody>
              {lists.map((list, index) => {
                return (
                  <tr>
                    <td>{list.name}</td>
                    <td>{list.creationDate}</td>
                    <td>{list.suppliersCount}</td>
                    <td>{list.description}</td>
                  </tr>
                );
              })}
            </tbody>
          </table>
        </div>
      </div>
    );
  }

  renderSuppliers(suppliers) {
    return (
      <div className="row mt-5">
        <p className="col-12 bluish">Suppliers</p>
        <div
          className="col-12 table-responsive shadowBox radius p-0"
          id="Table"
        >
          <table className="table m-0">
            <thead>
              <tr>
                <th>Name</th>
                <th>Email</th>
                <th>Phone</th>
                <th>Activity</th>
                <th>Joining Date</th>
              </tr>
            </thead>
            <tbody>
              {suppliers.map((supplier, index) => {
                return (
                  <tr>
                    <td>{supplier.username}</td>
                    <td>{supplier.email}</td>
                    <td>{supplier.phone}</td>
                    <td>{supplier.ActivityNames.join(", ")}</td>
                    <td>{supplier.joiningDate}</td>
                  </tr>
                );
              })}
            </tbody>
          </table>
        </div>
      </div>
    );
  }

  renderCommittees(committees) {
    let generateLink = (item, txt) => {
      return (
        <td>
          <Link to={`/committees/${item.id}/meetings`}>{txt}</Link>
        </td>
      );
    };

    return (
      <div className="row mt-5">
        <p className="col-12 bluish">Committees</p>
        <div
          className="col-12 table-responsive shadowBox radius p-0"
          id="Table"
        >
          <table className="table m-0">
            <thead>
              <tr>
                <th>Name</th>
                <th>Creation Date</th>
                <th>Meetings</th>
                <th>Members</th>
                <th>Description</th>
              </tr>
            </thead>
            <tbody>
              {committees.map((committee, index) => {
                return (
                  <tr>
                    {generateLink(committee, committee.name)}
                    {generateLink(committee, committee.creationDate)}
                    {generateLink(
                      committee,
                      `${committee.meetingsCount || "No"} Meetings`
                    )}
                    {generateLink(
                      committee,
                      `${committee.membersCount || "No"} Members`
                    )}
                    {generateLink(committee, committee.description)}
                  </tr>
                );
              })}
            </tbody>
          </table>
        </div>
      </div>
    );
  }
}
